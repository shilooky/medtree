module.exports = {
    mode: "development",
    entry: {
        bundle: "./src/main.js"
    },
    output: {
        filename: "[name].js"
    },
    devtool : 'source-map',
    module: {
        rules:[
            { test: /\.html$/, loader: 'html-loader'},
            { test: /\.txt$/i, loader: 'raw-loader'}
        ]
    }
};
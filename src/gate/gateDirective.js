const gateTemplate = require('./gate.html');
const angularModule = require('../angular/angularModule');
angularModule.directive('gate', gateDirective);

function gateDirective() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        template: gateTemplate,
        controller: 'gateController',
        controllerAs: 'gateController'
    }
}
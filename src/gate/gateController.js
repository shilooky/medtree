const angularModule = require('../angular/angularModule');
angularModule.controller('gateController', gateController);
let tree = require('../tree/tree');

function gateController($scope) {
    function ctor(){
        $scope.next = next;
    }

    function next() {
        if (!tree.getAllQuestions()) {
            alert('Please upload a file first.');
        } else {
            $scope.clickedNext = true;
        }
    }

    ctor();
}


function toggleCruiseControl() {
    cruiseControlSetting.value = !cruiseControlSetting.value;
}

function getCruiseControl() {
    return cruiseControlSetting.value;
}

function toggleShowRuleOutsPopup() {
    showRuleOutsPopupSetting.value = !showRuleOutsPopupSetting.value;
}

function getShowRuleOutsPopup() {
    return showRuleOutsPopupSetting.value;
}

const CRUISE_CONTROL_PARAMETER = "Cruise Control";
const DISPLAY_RULE_OUTS_PARAMETER = "Display Rule Outs Popup";

let cruiseControlSetting = {value: false, name: CRUISE_CONTROL_PARAMETER, onChanged: toggleCruiseControl};
let showRuleOutsPopupSetting = {value: true, name: DISPLAY_RULE_OUTS_PARAMETER, onChanged: toggleShowRuleOutsPopup};
let settings = [cruiseControlSetting, showRuleOutsPopupSetting];

function getSettingsObject() {
    return settings;
}

module.exports = {getSettingsObject, getShowRuleOutsPopup, getCruiseControl, toggleCruiseControl, toggleShowRuleOutsPopup};



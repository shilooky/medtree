let ruleEvaluator = require('./ruleEvaluator')

function evaluateActions(question) {
    if (!question.ruleOutRules) 
        return;

    let appliedRuleOutIds = [];
    for (let ruleOutRuleIndex in question.ruleOutRules) {
        let ruleOutRule = question.ruleOutRules[ruleOutRuleIndex];
        let isRuleTrue = ruleEvaluator.isRuleTrue(ruleOutRule);

        if (isRuleTrue) {
            let actualTrueRules = getTrueRules(ruleOutRule);
            appliedRuleOutIds.push({isApplied: isRuleTrue, ruleOutId: ruleOutRule.nextId, reasons: actualTrueRules, displayReason: ruleOutRule.displayReason});
        }

        // TODO: ? this causes a lot of mess..
        // // Even if the GLOBAL rule doesn't match, we add the SUBrules that do match
        // let actualTrueRules = getTrueRules(ruleOutRule);
        // appliedRuleOutIds.push({isApplied: isRuleTrue, ruleOutId: ruleOutRule.nextId, reasons: actualTrueRules});
    }
    return appliedRuleOutIds;
}

// function getTrueRules(ruleOutRule) {
//     if (!ruleOutRule.subRules)
//         return [ruleOutRule];

//     let trueRules = [];
//     let setStrengthOnSubrule = false;
//     for (let index in ruleOutRule.subRules) {
//         let r = ruleOutRule.subRules[index];
//         if (ruleEvaluator.isRuleTrue(r)) {
//             trueRules.push(r);

//             if (!setStrengthOnSubrule && !r.strength) { // We only want to count it once.
//                 setStrengthOnSubrule = true;
//                 r.strength = ruleOutRule.strength;
//             } else if (!setStrengthOnSubrule && ruleOutRule.strength) {
//                 console.log("This is probly a bug where we set the strength on a subrule that has been set before (from a different question i guess?). potentially can be only 1 subrule, and then we miss a strength count");
//             }

//             // TODO: "or" should show everything?
//             // if (ruleOutRule.logic === "or") // This is "or" - we only need one.
//             //     break;
//         }
//     }

//     return trueRules;
// }

function getTrueRules(ruleOutRule) {
    if (!ruleOutRule.subRules)
        return [ruleOutRule];

    let trueRules = [];
    let setStrengthOnSubrule = false;
    for (let index in ruleOutRule.subRules) {
        let r = ruleOutRule.subRules[index];
        if (r.subRules) {
            let trueSubrules = getTrueRules(r);
            trueSubrules.forEach(r => {
                r.displayReason = ruleOutRule.displayReason;
                trueRules.push(r);
            });
        } else {
            if (ruleEvaluator.isRuleTrue(r)) {
                r.displayReason = ruleOutRule.displayReason;
                trueRules.push(r);

                if (!setStrengthOnSubrule && (!r.strength && r.strength!=0)) { // We only want to count it once.
                    setStrengthOnSubrule = true;
                    r.strength = ruleOutRule.strength;
                } else if (!setStrengthOnSubrule && (ruleOutRule.strength)) {
                    console.log("This is probly a bug where we set the strength on a subrule that has been set before (from a different question i guess?). potentially can be only 1 subrule, and then we miss a strength count");
                }

                // TODO: "or" should show everything?
                // if (ruleOutRule.logic === "or") // This is "or" - we only need one.
                //     break;
            }
        }
    }
    return trueRules;
}

module.exports = {evaluateActions}
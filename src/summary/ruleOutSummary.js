const ruleEvaluator = require('../ruleEvaluator');

const SENTENCE_DELIMITER = " ";

function generate(ruleOut) {
    let sentencesToApply = ruleOut.ruleOutText.filter(ruleOutText => {
        return ruleEvaluator.isRuleTrue(ruleOutText.rule);
    });

    let summary = "";
    sentencesToApply.forEach(ruleOutText => {
        summary += ruleOutText.text + SENTENCE_DELIMITER;
    });
    return summary
}

module.exports = {generate};
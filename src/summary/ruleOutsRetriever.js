const summary = require('./summary');

function getRuleOuts() {
    return summary.getRuleOuts();
}

module.exports = {getRuleOuts};
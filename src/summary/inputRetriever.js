const tree = require('../tree/tree');

function getInput(id) {
    return tree.getInput(id);
}

function getAllInputs() {
    return tree.getAllInputs();
}

module.exports = {getInput, getAllInputs};
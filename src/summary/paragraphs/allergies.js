const answerRetriever = require('../answerRetriever');

function create() {
    let story = "";
    let g6 = answerRetriever.getAnswer("g6");
    if (g6 && g6 == "כן") {
        story += "מחסור ב G6PD, ";
    }

    let g7 = answerRetriever.getAnswer("g7");
    let g7_1 = answerRetriever.getAnswer("g7_1");
    if (g7 == "כן") {
        story += "לפיו דבריו/דבריה אלרגי ל: " + g7_1 + ", ";
    }
    return story
}

module.exports = {create};
let stroke = require('./stroke');
const answerRetriever = require('../answerRetriever');
const ruleOutsRetriever = require('../ruleOutsRetriever');
const tree = require('../../tree/tree')
const RuleOutProbability = require('../RuleOutProbability');
const ruleEvaluator = require('../../ruleEvaluator');

function createAlerts() {
    return create("alert");
}

function createRuleOuts() {
    return create("rule-out");
}

function createActions() {
    return create("action").sort(function(action1, action2){
        return action1.order - action2.order;
    });
}

function create(type) {
    let ruleOuts = [];
    let allRuleOuts = ruleOutsRetriever.getRuleOuts();
    for (let index in allRuleOuts) {
        let ruleOut = allRuleOuts[index];

        if (ruleOut.type != type)
            continue;

        let strengthScore = calculateStrength(ruleOut);
        let strengthText = getStrengthDescription(strengthScore);
        
        let reasons = [];
        let questionToRelevantAnswersMap = {}
        if (ruleOut.reasons) {
            ruleOut.reasons.forEach(reason => {
                if (reason.displayReason) {
                    addReason(reason, questionToRelevantAnswersMap);
                }
            }); 
        }
        let delimiter = ", ";
        for (let questionStr in questionToRelevantAnswersMap) {
            relevantAnswers = questionToRelevantAnswersMap[questionStr];

            let relevantAnswersText = "";
            relevantAnswers.forEach(a => relevantAnswersText += a + delimiter);
            relevantAnswersText = relevantAnswersText.substring(0, relevantAnswersText.length-delimiter.length);
            
            reasons.push({questionStr: questionStr, answerStr: relevantAnswersText});
        }

        let scoreObject = undefined;
        if (ruleOut.score && (ruleOut.score.value || ruleOut.score.value==0)) {
            // This is because we don't have dummy-proofing. we calculate again in case anything changed since last time
            let ruleOutScore = calculateRuleOutScore(ruleOut);
            if ((ruleOutScore || ruleOutScore==0) && ruleOut.score) {
                ruleOut.score.value = ruleOutScore;
            }

            // scoreValue = ruleOut.score.value;
            scoreObject = ruleOut.score;
        }

        ruleOuts.push({id: ruleOut.id, name: ruleOut.questionStr, reasons: reasons, strengthScore: strengthScore, strengthText: strengthText, score: scoreObject, isConfirmed: ruleOut.isConfirmed, originalNode: ruleOut, order: ruleOut.actionOrder, actionSection: ruleOut.actionSection, confirmed: ruleOut.confirmed, moveToTop: ruleOut.moveToTop});
    }

    ruleOuts.sort(function(a, b) {
        let aStrength = 0;
        let bStrength = 0;
        if (a.strengthScore)
            aStrength = a.strengthScore;
        if (b.strengthScore)
            bStrength = b.strengthScore;

        return bStrength - aStrength;
    });
    return ruleOuts;
}

// TODO: This is a duplicate of the function in questionController... make this some module
function calculateRuleOutScore(ruleOut) {
    if (!ruleOut.score || !ruleOut.score.values || ruleOut.score.values.length == 0)
        return;

    let questionToRelevantAnswersMap = {};
    let questionToScoreAdditionMap = {};
    let score = 0;
    ruleOut.score.values.forEach(v => {
        if (!v.rule || !v.value)
            return;
        
        if (ruleEvaluator.isRuleTrue(v.rule)) {
            score += v.value;
            addReason(v.rule, questionToRelevantAnswersMap);
            addScoreAmount(v.rule, questionToScoreAdditionMap, v.value);
        }
    });

    ruleOut.score.reasons = [];
    let delimiter = ", ";
    for (let questionStr in questionToRelevantAnswersMap) {
        relevantAnswers = questionToRelevantAnswersMap[questionStr];

        let relevantAnswersText = "";
        relevantAnswers.forEach(a => relevantAnswersText += a + delimiter);
        relevantAnswersText = relevantAnswersText.substring(0, relevantAnswersText.length-delimiter.length);
        
        let scoreAmount = questionToScoreAdditionMap[questionStr];
        ruleOut.score.reasons.push({questionStr: questionStr, answerStr: relevantAnswersText, scoreAmount: scoreAmount});
    }

    return score;
}

function addScoreAmount(reason, questionToScoreAdditionMap, scoreAmountToAdd) {
    if (reason.condition) {
        let condition = reason.condition;
        let question = tree.getQuestion(condition.questionId);
        let operatorText = "";
        
        if (condition.operator) { // Operator
            let operatorValue = parseInt(condition.operatorValue);
            if (condition.operator === ">") {
                operatorText += "גדול מ-"
            } else if (condition.operator === ">=") {
                operatorText += "גדול או שווה ל-"
            } else if (condition.operator === "<") {
                operatorText += "קטן מ-"
            } else if (condition.operator === "<=") {
                operatorText += "קטן או שווה ל-"
            } else if (condition.operator === "=") {}
        }

        let isInput = false;
        let isRuleOut = false;
        if (!question) {
            question = tree.getInput(condition.questionId);
            if (question) {
                isInput = true;
            }
        }
        if (!question) {
            question = tree.getRuleOut(condition.questionId);
            if (question) {
                isRuleOut = true;
            }
        }

        if (question) {
            let questionStr = question.doctorDisplayText || question.questionStr;
            questionToScoreAdditionMap[questionStr] = scoreAmountToAdd;
        }
    }
    else if (reason.logic == "visited node" && reason.visitedNode) {
        let nodeId = reason.visitedNode.nodeKey;
        let node = tree.getQuestion(nodeId);
        if (!node) {
            node = tree.getInput(nodeId);
        }
        if (!node) {
            node = tree.getRuleOut(nodeId);
        }

        if (node) {
            let questionStr = node.doctorDisplayText || node.questionStr;
            if (reason.visitedNode.not) {
                questionStr = "אין " + questionStr;
            }
            if (questionToScoreAdditionMap[questionStr]) {
                questionToScoreAdditionMap[questionStr] += scoreAmountToAdd;
            } else {
                questionToScoreAdditionMap[questionStr] = scoreAmountToAdd;
            }
        }
    }
}

function calculateStrength(ruleOut) {
    // TODO: What?
    if (ruleOut.strength || ruleOut.strength == 0)
        return ruleOut.strength;

    let strengthCounterMap = {};
    strengthCounterMap[RuleOutProbability.RuleOutProbability.VERY_WEAK] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.WEAK] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.NEUTRAL] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.STRONG] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.VERY_STRONG] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.MUST] = 0;
    let foundStrength = false;
    if (ruleOut.reasons) {
        ruleOut.reasons.forEach(reason => {
            if (reason.strength || reason.strength==0) {
                let strengthOrdinal = reason.strength;
                let strengthEnum = RuleOutProbability.get(strengthOrdinal);
                if (strengthCounterMap[strengthEnum] || strengthCounterMap[strengthEnum] == 0) {
                    strengthCounterMap[strengthEnum]++;
                    foundStrength = true;
                }
            }
        }); 
    }

    if (!foundStrength)
        return null;

    let finalStrengthScore = strengthCounterMap[RuleOutProbability.RuleOutProbability.VERY_WEAK]*(VERY_WEAK_SCORE) +
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.WEAK]*(WEAK_SCORE) +
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.NEUTRAL]*NEUTRAL_SCORE +
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.STRONG]*STRONG_SCORE +
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.VERY_STRONG]*VERY_STRONG_SCORE + 
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.MUST]*MUST_SCORE;
    return finalStrengthScore;
}

// const WEAK_SCORE = -10;
// const STRONG_SCORE = 10;
// const VERY_STRONG_SCORE = 20;
// const MUST_SCORE = 100;

const VERY_WEAK_SCORE = -20;
const WEAK_SCORE = -10;
const NEUTRAL_SCORE = 0;
const STRONG_SCORE = 10;
const VERY_STRONG_SCORE = 22;
const MUST_SCORE = 50;

// TODO:
// const VERY_HIGH_MIN_SCORE = 70;
const VERY_HIGH_MIN_SCORE = 100;
const HIGH_MIN_SCORE = 51;
const MEDIUM_MIN_SCORE = 41;

function getStrengthDescription(strengthScore) {
    if (!strengthScore)
        return null;

    if (strengthScore < MEDIUM_MIN_SCORE) // lower than MEDIUM_MIN_SCORE (low)
        return RuleOutProbability.WEAK_TEXT;

    if (strengthScore < HIGH_MIN_SCORE) // between MEDIUM_MIN_SCORE and HIGH_MIN_SCORE (medium)
        return RuleOutProbability.STRONG_TEXT;

    if (strengthScore < VERY_HIGH_MIN_SCORE) // between HIGH_MIN_SCORE and VERY_HIGH_MIN_SCORE (high)
        return RuleOutProbability.VERY_STRONG_TEXT;

    if (strengthScore >= VERY_HIGH_MIN_SCORE) // higher than VERY_HIGH_MIN_SCORE (very high)
        return RuleOutProbability.MUST_TEXT;

    return RuleOutProbability.UNKNOWN_STRENGTH_TEXT;


    // if (score < STRONG_SCORE) // between weak and strong
    //     return RuleOutProbability.WEAK_TEXT;
    
    // if (score < VERY_STRONG_SCORE) // between strong and very strong
    //     return RuleOutProbability.STRONG_TEXT;

    // if (score < MUST_SCORE) // between very strong and must
    //     return RuleOutProbability.VERY_STRONG_TEXT;

    // if (score >= MUST_SCORE)
    //     return RuleOutProbability.MUST_TEXT;
}

function addReason(reason, questionToRelevantAnswersMap) {
    if (reason.condition) {
        // TODO: for dont display reasons
        // TODO: if (reason.condition.dontDisplay) return;

        let condition = reason.condition;
        let question = tree.getQuestion(condition.questionId);
        let operatorText = "";
        let suffixText = "";
        // if (condition.not && condition.operator) {
        //     operatorText += "לא ";
        // } else if (condition.not) {
        //     operatorText += "שונה מ-";
        // }

        // if (condition.not) {
        //     operatorText += "לא ";
        // }
        
        // if (condition.operator) { // Operator
        //     let operatorValue = parseInt(condition.operatorValue);
        //     if (condition.operator === ">") {
        //         operatorText += "גדול מ-"
        //     } else if (condition.operator === ">=") {
        //         operatorText += "גדול או שווה ל-"
        //     } else if (condition.operator === "<") {
        //         operatorText += "קטן מ-"
        //     } else if (condition.operator === "<=") {
        //         operatorText += "קטן או שווה ל-"
        //     } else if (condition.operator === "=") {}
        // }

        let isInput = false;
        let isRuleOut = false;
        if (!question) {
            question = tree.getInput(condition.questionId);
            if (question) {
                isInput = true;
            }
        }
        if (!question) {
            question = tree.getRuleOut(condition.questionId);
            if (question) {
                isRuleOut = true;
            }
        }

        if (question) {
            let answer = condition.answer; // TODO: This isn't really good, we don't write "he answered 6", we write "he answered more than 5".. 
            let questionStr = question.doctorDisplayText || question.questionStr;

            if (!answer || condition.operator/* || condition.not*/) {
                // if (condition.operator) {
                //     operatorText += condition.operatorValue + " (";
                // } 
                
                answer = answerRetriever.getAnswer(question.id);

                if (!answer && isInput) { // Let's pretend this is an Input
                    let parameter = question.answers.find(a => a.str === condition.answer)
                    if (parameter) {
                        answer = parameter.value;
                        questionStr = parameter.str;
                        if (parameter.type == "radio") {
                            questionStr = getRadioTitle(questionStr);
                        }
                    }
                }

                if (Array.isArray(answer)) {
                    let answerText = "";
                    let delimiter = ", ";
                    answer.forEach(a => answerText += a.str+delimiter)
                    answerText = answerText.substring(0, answerText.length-delimiter.length);
                    answer = answerText;
                }
                //suffixText = ")"
            }

            if (condition.not) {
                answer = answerRetriever.getAnswer(question.id);
                if (Array.isArray(answer)) { // TODO: This is stupido
                    let answerText = "";
                    let delimiter = ", ";
                    answer.forEach(a => answerText += a.str+delimiter)
                    answerText = answerText.substring(0, answerText.length-delimiter.length);
                    answer = answerText;
                }
            }

            if (answer == null) {
                answer = "";
            }

            let actualDescriptionText = operatorText+answer;//+suffixText;
            if (condition.operator && condition.operator === "=") { // If it's an "=" operator, we have an operator but it's straight-forward - we don't need the "(value)" text
                actualDescriptionText = answer;
            }

            if (questionToRelevantAnswersMap[questionStr] && !condition.not) { // NOT goes through all the array, so we don't need it to happen twice\
                questionToRelevantAnswersMap[questionStr].push(actualDescriptionText);
                // questionToRelevantAnswersMap[questionStr] = [...new Set(questionToRelevantAnswersMap[questionStr])];
            } else {
                questionToRelevantAnswersMap[questionStr] = [actualDescriptionText];
            }
        } else { // We got here from a ruleout/action
            // TODO: Maybe just take the reasons for the ruleout/action that brought us here?
            console.log("Got a Rule/Action with no condition: logic=" + reason.logic + ", condition={questionId="+condition.questionId+", answer=" + condition.answer + "}");
        }
    } 
    else if (reason.logic == "visited node" && reason.visitedNode) {
        let nodeId = reason.visitedNode.nodeKey;
        let node = tree.getQuestion(nodeId);
        if (!node) {
            node = tree.getInput(nodeId);
        }
        if (!node) {
            node = tree.getRuleOut(nodeId);
        }

        if (node) {
            let questionStr = node.questionStr;
            if (reason.visitedNode.not) {
                questionStr = "אין " + node.questionStr;
            }
            questionToRelevantAnswersMap[questionStr] = []; // Just add the visited node name and that's it
        }
    }
    else {
        if (reason.logic == "or") {
            if (reason.subRules && reason.subRules.length > 0) {
                // addReason(reason.subRules[0], questionToRelevantAnswersMap); // This is "or" - just add the first one
                // TODO: "or" should show everything?
                reason.subRules.forEach(subRule => addReason(subRule, questionToRelevantAnswersMap));
            }
        } else if (reason.logic == "and") {
            if (reason.subRules) {
                reason.subRules.forEach(subRule => addReason(subRule, questionToRelevantAnswersMap));
            }
        } else {
            // TODO: logic == "default" ? what should we do in this case
            if (reason.logic != "default") {
                console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ no condition on the reason? - " + reason.logic)
            }
        }
    }
}

// TODO: This is copied from questionController - make a module out of this and re-use
const RADIO_TITLE_DELIMITER = "::";
function getRadioTitle(parameterString) {
    let endIndex = parameterString.indexOf(RADIO_TITLE_DELIMITER);
    if (endIndex != -1) {
        return parameterString.substring(0, endIndex);
    }
}

// //אבחנה מבדלת
// function create() {
//     let low = "סבירות נמוכה- ";
//     let high = "סבירות גבוהה- ";

//     let strokebranch = answerRetriever.getAnswer("strokebranch");
//     if (strokebranch == "לא") {
//         high += "TIA, ";
//     }

//     if (stroke.isStroke()) {
//         low +="גידול מוחי, wernicke-encephalopathy, hepathic-encephalopathy, אבצס מוחי, מחלה מטוכונדריאלית, היפוגליקמיה, ";
//         high += "שבץ-cva, ";
//     }

//     let g1_1 = answerRetriever.getAnswer("g1_1");
//     if (g1_1 == "כן") {
//         low += "ספסיס, ";
//     } 

//     let g1_2 = answerRetriever.getAnswer("g1_2");
//     if (g1_2 == "כן") {
//         low += "גיליאן ברה, ";
//     } 

//     let g1_5 = answerRetriever.getAnswer("g1_5");
//     if (g1_5 == "כן") {
//         low += "מנינגיטיס, ";
//     }

//     return high + "\n" + low;
// }

module.exports = {createRuleOuts, createActions, createAlerts};
const answerRetriever = require('../answerRetriever');

function create() {
    let story = "";
    let hadStroke = "";
    let without = "";
    let extra = "";
    let qFAST1 = answerRetriever.getAnswer("qFAST1");
    if (qFAST1 == "כן") {
        story += "מתלונן על חולשה ביד/רגל/פנים, ";
    }

    let qFAST2 = answerRetriever.getAnswer("qFAST2");
    if (qFAST2 == "כן") {
        story += "המיפארזיס, ";
    }

    let qFAST3 = answerRetriever.getAnswer("qFAST3");
    if (qFAST3 == "כן") {
        story += "דיספזיה, ";
    }

    let strokebranch = answerRetriever.getAnswer("strokebranch");
    if (strokebranch == "כן") {
        story += "משך הסימפטומים יותר מ24 שעות, ";
    } else if (strokebranch == "לא"){
        story += "משך הסימפטומים פחות מ24 שעות, ";
    }
    
    let q2000_1 = answerRetriever.getAnswer("q2000_0");
    if (q2000_1 == "כן") {
        story += "החל באופן פתאומי, ";
    } else if (q2000_1 == "לא") {
        story += "החל באופן הדרגתי, ";
    }

    let q2000_0 = answerRetriever.getAnswer("q2000_0");
    if (q2000_0 == "כן") {
        story += "דיספגיה, ";
    } else if (q2000_0 == "לא") {
        without += "ללא דיספגיה, ";
    }

    let q2000 = answerRetriever.getAnswer("q2000");
    if (q2000 == "כן") {
        story += "היה פרכוס, ";
    } else if (q2000 == "לא") {
        without += "ללא פרכוס, ";
    }

    let q2001 = answerRetriever.getAnswer("q2001");
    if (q2001 == "כן") {
        story += "מדמם כעת, ";
    } else if (q2001 == "לא") {
        without += "ללא דימום חיצוני, ";
    }

    let q2002 = answerRetriever.getAnswer("q2002");
    if (q2002 == "כן") {
        story += "עם אירוע של איבוד ההכרה, ";
    } else if (q2002 == "לא") {
        without += "ללא איבוד הכרה,  ";
    }

    let q2003 = answerRetriever.getAnswer("q2003");
    if (q2003 == "כן") {
        story += "מדווח על כאב ראש, ";
    } else if (q2003 == "לא") {
        without += "שולל כאב ראש,  ";
    }

    let q2010 = answerRetriever.getAnswer("q2010");
    if (q2010 == "כן") {
        story += "יש פוטופוביה, ";
    } else if (q2010 == "לא") {
        without += "ללא פוטופוביה, ";
    }

    let q2011 = answerRetriever.getAnswer("q2011");
    if (q2011 == "כן") {
        story += "יש נוקשות צווארית, ";
    } else if (q2011 == "לא") {
        without += "שולל נוקשות צווארית, ";
    }

    let q2012 = answerRetriever.getAnswer("q2012");
    if (q2012 == "כן") {
        story += "יש בחילה, ";
    } else if (q2012 == "לא") {
        without += "ללא בחילה, ";
    }

    let q2006 = answerRetriever.getAnswer("q2006");
    if (q2006 == "כן") {
        hadStroke += "היה אירוע מוחי בעבר, ";
    } else if (q2006 == "לא") {
        hadStroke += "שולל אירוע מוחי בעבר, ";
    }

    let q2008 = answerRetriever.getAnswer("q2008");
    if (q2008 == "לא") {
        without += "שולל הפרעות בקצב לב, ";
    }

    let q2009 = answerRetriever.getAnswer("q2009");
    if (q2009 == "לא") {
        story += "שולל ממחלות קרישת דם, ";
    }

    let q2014 = answerRetriever.getAnswer("q2014");
    if (q2014 == "כן") {
        hadStroke += "עבר ניקור מותני בשבוע האחרון, ";
    } else if (q2014 == "לא") {
        hadStroke += "לא עבר ניקור מותני בשבוע האחרון,  ";
    }
    
    let q2015 = answerRetriever.getAnswer("q2015");
    if (q2015 == "לא") {
        without += "שולל אירוע לבבי ופריקרדיטיס בעבר, ";
    }

    let q2016 = answerRetriever.getAnswer("q2016");
    if (q2016 == "לא") {
        without += "אינו נוטל מדללי דם או נוגדי טסיות, ";
    }

    let q2017 = answerRetriever.getAnswer("q2017");
    if (q2017 == "לא") {
        without += "שולל דימום פנימי בשישה שבועות האחרונים, ";
    }

    let g1_1 = answerRetriever.getAnswer("g1_1");
    if (g1_1 == "כן") {
        story += " יש חום/בלבול, ";
    } else if (g1_1 == "לא") {
        without += "ללא חום ללא בלבול,  ";
    }

    let g1_2 = answerRetriever.getAnswer("g1_2");
    if (g1_2 == "כן") {
        story += " מתלונן על קלקול קיבה, ";
    } else if (g1_2 == "לא") {
        without += "שולל קלקול קיבה, ";
    }

    let g1_3 = answerRetriever.getAnswer("g1_3");
    if (g1_3 == "כן") {
        story += "יש תלונות אורינריות, ";
    } else if (g1_3 == "לא") {
        without += "ללא צריבה במתן שתן, ";
    }

    let g1_5 = answerRetriever.getAnswer("g1_5");
    if (g1_5 == "כן") {
        story += "יש פריחה עורית, ";
    } else if (g1_5 == "לא") {
        without += "ללא פריחה עורית, ";
    }
    
    let g1_14 = answerRetriever.getAnswer("g1_14");
    if (g1_14 == "כן") {
        story += "מיאלגיה, ";
    } else if (g1_14 == "לא")  {
        extra += "ללא מיאלגיה, ";
    }

    let g1_16 = answerRetriever.getAnswer("g1_16");
    if (g1_16 == "כן") {
        story += "יש זיהום, ";
    } else if (g1_16 == "לא") {
        without += "שולל זיהום, ";
    }
    
    let g1_0 = answerRetriever.getAnswer("g1_0");
    if (g1_0 == "סיבה אחרת") {
        extra = "\nבנוסף, מתלונן על " + answerRetriever.getAnswer("g1_0_1") + ", ";
    } else if (g1_0 && g1_0 != "סיבה אחרת") {
        extra = "\nבנוסף, מתלונן על, " +g1_0;
    }
    
    let g1_4 = answerRetriever.getAnswer("g1_4");
    if (g1_4 == "לא") {
        without += "ללא רקע של דיכוי חיסוני, ";
    }

    let g1_6 = answerRetriever.getAnswer("g1_6");
    if (g1_6 == "כן") {
        story += "נסיעה לחול לאחרונה, ";
    } else if (g1_6 == "לא"){
        without += "ללא נסיעה לחול לאחרונה, ";
    }

    let g1_7 = answerRetriever.getAnswer("g1_7");
    if (g1_7 == "לא") {
        without += "ללא רקע של מייסתניה גרוויס, ";
    }

    let g1_8 = answerRetriever.getAnswer("g1_8");
    if (g1_8 == "לא") {
        without += "ללא רקע של מחלת כליות, ";
    }

    let g1_9 = answerRetriever.getAnswer("g1_9");
    if (g1_9 == "כן") {
        extra += "חיסונים כשגרה, ";
    } else if (g1_9 == "לא") {
        extra += "ללא חיסונים כשגרה, ";
    }

    let g1_13 = answerRetriever.getAnswer("g1_13");
    if (g1_13 == "לא") {
        without += "ללא רקע של מחלות בבלוטת התריס, ";
    }

    return story + hadStroke + "\n\n" + without + extra;
}

function isStroke() {
    let qFAST1 = answerRetriever.getAnswer("qFAST1");
    let qFAST2 = answerRetriever.getAnswer("qFAST2");
    let qFAST3 = answerRetriever.getAnswer("qFAST3");
    return (qFAST1 == "כן" || qFAST2 == "כן" || qFAST3 == "כן");
}

module.exports = {create, isStroke};
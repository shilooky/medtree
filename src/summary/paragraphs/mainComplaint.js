const answerRetriever = require('../answerRetriever');

function mainComplaint() {
    let qmaincomplaint = answerRetriever.getAnswer("qmaincomplaint");
    if (qmaincomplaint == "סיבה אחרת") {
        return answerRetriever.getAnswer("qmaincomplaint_1");
    }
    return qmaincomplaint || "___";
}

function age() {
    let genderAnswer = answerRetriever.getAnswer('gender');
    let ageAnswer = answerRetriever.getAnswer('age');

    if (genderAnswer == "זכר") {
        return `בן ${ageAnswer}`;
    } else if (genderAnswer == "נקבה") {
        return `בת ${ageAnswer}`;
    }

    return "";
}

function gcs() {
    let openEyes = answerRetriever.getAnswer("openEyes");
    let movement = answerRetriever.getAnswer("movement");
    let sentences = answerRetriever.getAnswer("sentences");

    if (!openEyes || !movement || !sentences) {
        return "";
    }

    let score = 0
    let extra = "";
    
    if (openEyes == "כן") {
        score += 3;
    } else {
        extra += " לא פוקח עיניים, "
    }
    if (sentences == "כן") {
        score += 4;
    } else {
        extra += "פגיעה בתגובה ורבלית, "
    }
    if (movement == "כן") {
        score +=6
    } else {
        extra += "פגיעה בתגובה מוטורית, "
    }
    
    if (score == 13) {
        return "estimated GCS >= 13" + extra;
    }

    return "estimated GCS < 13";
}

function social() {
    let story = "";
    let g12 = answerRetriever.getAnswer("g12");
    if (g12 == "כן") {
        story += "נשוי ";
    } else if (g12 == "לא") {
        story += "רווק ";
    }

    let g13 = answerRetriever.getAnswer("g13");
    let g13_1 = answerRetriever.getAnswer("g13_1");
    if (g13 == "כן") {
        story += "+ " + g13_1 + " ";
    } else if (g13 == "לא") {
        story += "ללא ילדים, ";
    }

    let g11 = answerRetriever.getAnswer("g11");
    if (g11) {
        story += "ביומיום " + g11 + ", ";
    }
    
    let g15 = answerRetriever.getAnswer("g15");
    if (g15 == "כן - עצמאי") {
        story += "עצמאי, ";
    }
    if (g15 == "זקוק לעזרה ביומיום") {
        story += "אינו עצמאי, ";
    }
    return story;
}
module.exports = {mainComplaint, age, gcs, social};
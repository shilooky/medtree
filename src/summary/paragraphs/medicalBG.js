const answerRetriever = require('../answerRetriever');

function create() {
    let story = "";
    let q2008 = answerRetriever.getAnswer("q2008");
    if (q2008 == "כן") {
        story += "ידוע הפרעות בקצב הלב, ";
    }

    let q2009 = answerRetriever.getAnswer("q2009");
    if (q2009 == "כן") {
        story += "ידוע על מחלות קרישת דם, ";
    }

    let q2013 = answerRetriever.getAnswer("q2013");
    if (q2013 == "כן") {
        story += "ידוע על כליה פוליציסטית, ";
    }

    let q2015 = answerRetriever.getAnswer("q2015");
    if (q2015 == "כן") {
        story += "רקע קרדיאלי חיובי, ";
    }

    let q2017 = answerRetriever.getAnswer("q2017");
    if (q2017 == "כן") {
        story += "יש דימום פנימי בשישה שבועות האחרונים, ";
    }
    
    let g1_4 = answerRetriever.getAnswer("g1_4");
    if (g1_4 == "כן") {
        story += "יש רקע של דיכוי חיסוני, ";
    }

    let g1_7 = answerRetriever.getAnswer("g1_7");
    if (g1_7 == "כן") {
        story += "רקע של מייסתניה גרוויס, ";
    }

    let g1_8 = answerRetriever.getAnswer("g1_8");
    if (g1_8 == "כן") {
        story += "רקע של מחלת כליות, ";
    }

    let g1_13 = answerRetriever.getAnswer("g1_13");
    if (g1_13 == "כן") {
        story += "רקע של מחלות תיירואיד, ";
    }

    let g1_17 = answerRetriever.getAnswer("g1_17");
    if (g1_17 == "כן") {
        story += "עבר ניתוח מוח בעבר, ";
    }

    let g2 = answerRetriever.getAnswer("g2");
    let g2_1 = answerRetriever.getAnswer("g2_1");
    if (g2) {
        story += g2 + " "
    }

    if (g2_1) {
        story += g2_1 + " "
    }

    let gDDX1_1 = answerRetriever.getAnswer("gDDX1_1");
    if (gDDX1_1) {
        story += gDDX1_1 + ", ";
    }
    
    let g3 = answerRetriever.getAnswer("g3");
    if (g3 && g3 != "לא") {
        story += "במעקב של רופא " + g3 + ", ";
    }

    let gDDX3 = answerRetriever.getAnswer("gDDX3");
    if (gDDX3 && gDDX3 == "כן") {
        story += "רקע של מחלת כבד, ";
    }

    return story;
}


module.exports = {create};
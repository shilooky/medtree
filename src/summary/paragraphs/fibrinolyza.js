const answerRetriever = require('../answerRetriever');
const stroke = require('./stroke');
//TODO: add names to questions in builder
function create() { //TODO: seperate by commas
    if (!stroke.isStroke()) {
        return "";
    }
    let story = "קונטרה אינדיקציה לפיברינוליזה?\n";
    let q2004 = answerRetriever.getAnswer("q2004");
    if (q2004 == "כן") {
        story += "בשלושה חודשים האחרונים עבר ניתוח בראש או עמוד שדרה, ";
    } else if (q2004 == "לא") {
        story += "בשלושה חודשים האחרונים לא עבר ניתוח בראש או עמוד שדרה,  ";
    }

    let q2005 = answerRetriever.getAnswer("q2005");
    if (q2005 == "כן") {
        story += "בשלושה חודשים האחרונים היה פגיעת ראש או שבץ מוחי, ";
    } else if (q2005 == "לא") {
        story += "בשלושה חודשים האחרונים לא היה פגיעת ראש או שבץ מוחי, ";
    }

    let q2006 = answerRetriever.getAnswer("q2006");
    if (q2006 == "כן") {
        story += "היה אירוע מוחי בעבר ";
    } else if (q2006 == "לא") {
        story += "לא היה אירוע מוחי בעבר ";
    }

    let q2007 = answerRetriever.getAnswer("q2007");
    if (q2007 == "כן") {
        story += "ידוע על פגם במוח, ";
    } else if (q2007 == "לא") {
        story += "לא ידוע על מלפורמציה וסקולרית ";
    }

    let q2007_1 = answerRetriever.getAnswer("q2007_1");
    if (q2007_1 == "כן") {
        story += "קיים גידול במוח, ";
    } else if (q2007_1 == "לא") {
        story += "שולל גידול/סרטן מוחי, ";
    }

    return story
}

module.exports = {create};
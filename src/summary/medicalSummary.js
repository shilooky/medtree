const anamnesisStoryTemplate = require('./template.txt').default;
const definitiveDiagnosis = require('./paragraphs/definitiveDiagnosis');
const answerRetriever = require('./answerRetriever');
const inputRetriever = require('./inputRetriever');
const ruleEvaluator = require('../ruleEvaluator');
const summary = require('./summary');
const inputState = require('../inputs/inputState');

function generate() {
    let allRuleOuts = definitiveDiagnosis.createRuleOuts();
    let ruleOuts = allRuleOuts.filter(ro => !ro.confirmed && !ro.moveToTop);
    let topRuleOuts = allRuleOuts.filter(ro => ro.moveToTop);
    let confirmedRuleOuts = allRuleOuts.filter(ro => ro.confirmed);
    return {
        ruleOuts, topRuleOuts, confirmedRuleOuts,
        alerts: definitiveDiagnosis.createAlerts(), 
        anamnesisStory: createAnamnesisStory(),
        actionSections: createActionParagraphs(definitiveDiagnosis.createActions())
    };
}

const ANSWER_VALUE_TEMPLATE_PREFIX = "[val=";
const ANSWER_VALUE_TEMPLATE_SUFFIX = "]";
const VALUE_DELIMITER = ", ";
const SENTENCE_DELIMITER = " ";
const ROW_DELIMITER = " ";

function createActionParagraphs(actions) {
    let actionParagraphs = {
        "בדיקה גופנית": {name: "בדיקה גופנית" , actions: []},
        "בדיקות מעבדה": {name: "בדיקות מעבדה" , actions: []},
        "דימות": {name: "דימות" , actions: []},
        "בדיקות אחרות" : {name: "בדיקות אחרות"  , actions: []},
        "טיפול": {name: "טיפול" , actions: []},
    }
    
    actions.forEach(action => {
        if (actionParagraphs[action.actionSection])
            actionParagraphs[action.actionSection].actions.push(action);
    });

    return actionParagraphs;
}

function createAnamnesisStory() {
    let storyRows = {};

    addInitialAnamnesis(storyRows);

    let inputs = inputRetriever.getAllInputs();
    for (let key in inputs) {
        let input = inputs[key];
        if (!input.anamnesis) 
            continue;

        input.anamnesis.forEach(anamnesis => addAnamnesis(storyRows, anamnesis));
    }

    // TODO: Maybe go over all visited nodes instead? there can be an anamnesis for R/O and Actions too..
    let questions = answerRetriever.getAllQuestionsAndAnswers();
    for (let key in questions) {
        let q = questions[key];
        if (!q.anamnesis)
            continue;

        q.anamnesis.forEach(anamnesis => addAnamnesis(storyRows, anamnesis));
    }

    let ruleOuts = answerRetriever.getAllRuleOuts(); // This is also actions and alrets
    for (let key in ruleOuts) {
        let ro = ruleOuts[key];
        if (!ro.anamnesis)
            continue;

        ro.anamnesis.forEach(anamnesis => addAnamnesis(storyRows, anamnesis));
    }

    for (let key in storyRows) {
        let row = storyRows[key];

        // Check if the suffix is the sentense delimiter.
        let rowSuffix = row.substring(row.length-SENTENCE_DELIMITER.length, row.length)
        if (rowSuffix == SENTENCE_DELIMITER) {
            row = row.substring(0, row.length-(SENTENCE_DELIMITER.length)); // Trim the last sentence delimiter
        }

        // Add the row delimiter
        storyRows[key] = row+ROW_DELIMITER;
    }

    return formatStoryRows(storyRows);
}

function addAnamnesis(storyRows, anamnesis) {
    let isApplied = true;
    // TODO: This is for empty rules with no condition - we can pretty confidently remove this when shiloh fixes the rules
    // if (anamnesis.rule && ((anamnesis.rule.condition && anamnesis.rule.condition.questionId) || !anamnesis.rule.condition)) { 
    if (anamnesis.rule) {
        isApplied = ruleEvaluator.isRuleTrue(anamnesis.rule);
    }

    if (isApplied) {
        let text = anamnesis.diagnosis;
        text = insertMissingValues(text)

        if (text && anamnesis.section) {
            if (!storyRows[anamnesis.section]) {
                storyRows[anamnesis.section] = "";
            }
            let delimiterText = text.trim().length > 0? SENTENCE_DELIMITER : "";
            storyRows[anamnesis.section] += text + delimiterText;
        }
    }
}

//TODO: this doess not support multiple values
function insertMissingValues(text) {
    if (!text.includes(ANSWER_VALUE_TEMPLATE_PREFIX)) {
        return text;
    }
    let startIndex = text.indexOf(ANSWER_VALUE_TEMPLATE_PREFIX);
    let endIndex = text.indexOf(ANSWER_VALUE_TEMPLATE_SUFFIX);
    let valuesToExtract = text.substring(startIndex+ANSWER_VALUE_TEMPLATE_PREFIX.length, endIndex).split(',');
    let questionId = valuesToExtract[0];
    let inputIndex = valuesToExtract[1];
    
    if (inputIndex) {
        val = getInputValue(questionId, inputIndex);
    } else {
        val = getQuestionValue(questionId);
    }

    if (val) {
        return text.substring(0, startIndex) + val + text.substring(endIndex + 1, text.length);
    }
}

function getInputValue(questionId, inputIndex) {
    let input = inputState.get();
}

function getQuestionValue(questionId) {
    let answer = answerRetriever.getAnswer(questionId);
    if (answer === undefined || answer === null) {
        return;
    }
    let answerText = "";
    if (Array.isArray(answer)) {
        answer.forEach(a => {
            if (a.selected) {
                answerText += a.str + VALUE_DELIMITER;
            }
        });
        answerText = answerText.substring(0, answerText.length-(VALUE_DELIMITER.length));
    } else {
        answerText = answer;
    }
    return answerText;
}

const MAIN_COMPLAINT_TITLE = "תלונה עיקרית";
const MAIN_COMPLAINT_1 = "תלונה עיקרית -שורה 1";
const MAIN_COMPLAINT_2 = "תלונה עיקרית -שורה 2";
const MAIN_COMPLAINT_3 = "תלונה עיקרית -שורה 3";

const COMPLAINT_BACKGROUND_TITLE = "רקע לתלונה";
const COMPLAINT_BACKGROUND_1 = "רקע לתלונה -שורה 1";
const COMPLAINT_BACKGROUND_2 = "רקע לתלונה -שורה 2";
const COMPLAINT_BACKGROUND_3 = "רקע לתלונה -שורה 3";
const COMPLAINT_BACKGROUND_4 = "רקע לתלונה -שורה 4";

const MEDICAL_BACKGROUND = "רקע רפואי";
const MEDICAL_BACKGROUND_DISPLAY_NAME = "רקע רפואי ומחלות"
const ALERGIES = "אלרגיות";
const MEDICATIONS = "תרופות";
const FORMER_HOSPITALIZATIONS = "אישפוזים קודמים";
const SOCIAL_BACKGROUND = "רקע סוציאלי";
const FAMILY_BACKGROUND = "רקע משפחתי";
const DD_HIGH = "אבחנה מבדלת - סבירות גבוהה";
const DD_LOW = "אבחנה מבדלת - סבירות נמוכה";
const RECOMMENDATIONS = "המלצות";
const BODY_CHECK = "בדיקה גופנית";
const LEB_TESTS = "בדיקות מעבדה";
const SCAN = "דימות";
const OTHER_CHECKS = "בדיקות אחרות";

function formatStoryRows(storyRows) {
    let mainComplaintParagraph = {title: MAIN_COMPLAINT_TITLE, rows: []};
    mainComplaintParagraph.rows.push(storyRows[MAIN_COMPLAINT_1]);
    mainComplaintParagraph.rows.push(storyRows[MAIN_COMPLAINT_2]);
    mainComplaintParagraph.rows.push(storyRows[MAIN_COMPLAINT_3]);

    let complaintBackgroundParagraph = {title: COMPLAINT_BACKGROUND_TITLE, rows: []};
    complaintBackgroundParagraph.rows.push(storyRows[COMPLAINT_BACKGROUND_1]);
    complaintBackgroundParagraph.rows.push(storyRows[COMPLAINT_BACKGROUND_2]);
    complaintBackgroundParagraph.rows.push(storyRows[COMPLAINT_BACKGROUND_3]);
    complaintBackgroundParagraph.rows.push(storyRows[COMPLAINT_BACKGROUND_4]);

    let paragraphs = [
        mainComplaintParagraph, 
        complaintBackgroundParagraph, 
    ];

    addParagraph(MEDICAL_BACKGROUND, storyRows, paragraphs)
    addParagraph(ALERGIES, storyRows, paragraphs)
    addParagraph(MEDICATIONS, storyRows, paragraphs)
    addParagraph(FORMER_HOSPITALIZATIONS, storyRows, paragraphs)
    addParagraph(SOCIAL_BACKGROUND, storyRows, paragraphs)
    addParagraph(FAMILY_BACKGROUND, storyRows, paragraphs)
    addParagraph(DD_LOW, storyRows, paragraphs)
    addParagraph(DD_LOW, storyRows, paragraphs)
    addParagraph(RECOMMENDATIONS, storyRows, paragraphs)
    addParagraph(BODY_CHECK, storyRows, paragraphs)
    addParagraph(LEB_TESTS, storyRows, paragraphs)
    addParagraph(SCAN, storyRows, paragraphs)
    addParagraph(OTHER_CHECKS, storyRows, paragraphs)

    eliminateUndefinedRows(paragraphs);
    return paragraphs;
}

function addParagraph(name, storyRows, paragraphs) {
    paragraph = {title: name, rows: []};
    paragraph.rows.push(storyRows[name]);
    paragraphs.push(paragraph);
}

// Eliminate rows that are undefined or empty (because it breaks the angular HTML)
function eliminateUndefinedRows(paragraphs) {
    paragraphs.forEach(p => {
        if (!p.rows)
            return;
        
        let allRowsOk = false;
        while (!allRowsOk) {
            let rowIndexesToRemove = [];
            for (let index in p.rows) {
                let r = p.rows[index];
                if (!r || r.trim().length==0) {
                    rowIndexesToRemove.push(index);
                }
            }

            if (rowIndexesToRemove.length == 0) {
                allRowsOk = true;
            } else {
                p.rows.splice(rowIndexesToRemove[0], 1); // we can only delete 1 index at a time..
            }
        }
    });
}

function formatByName(str, formats) {
    Object.keys(formats).forEach(name => {
        let val = formats[name];
        str = str.replace('$'+name+'$', val);
    })
    return str;
}

function addInitialAnamnesis(storyRows) {
    let state = inputState.get();
    let age = state['נתונים דמוגרפיים@@@_@@@גיל'];
    let gender = state['נתונים דמוגרפיים@@@_@@@מין::זכר;נקבה'];
    if (!age) {
        return;
    }
    if (gender == "זכר")
        storyRows[MAIN_COMPLAINT_1] = `גבר בן ${age}` + " ";
    else if (gender == "נקבה")
        storyRows[MAIN_COMPLAINT_1] = `אישה בת ${age}` + " ";
}


module.exports = {generate}
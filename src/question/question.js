const consts = require('../consts');

class Question {
    constructor(id, questionStr, options) {
        this.id = id;
        this.questionStr = questionStr;
        this.subTitle = options.subTitle;

        this.defaultNextId = options.defaultNextId;
        this.answers = (options && options.answers) || [];
        this.rules = options.rules || [];
        this.ruleOutRules = options.ruleOutRules || []; // This should be called "actions" - both ruleOuts and actions
        this.mode = options.mode || consts.questionMode.BUTTONS;
        this.end = options.end;
        this.score = options.score;
        this.multiChoiceYesNo = options.multiChoiceYesNo;
        this.allowSkip = options.allowSkip;
        this.anamnesis = options.anamnesis;
        this.message = options.message;
        this.shouldDisplayForDoctor = options.shouldDisplayForDoctor;
        this.doctorDisplayText = options.doctorDisplayText;
        this.alert = options.alert;
        this.ruleOutText = options.ruleOutText;
        this.actionOrder = options.actionOrder;
        this.actionSection = options.actionSection;
        this.isFirst = options.isFirst;
        this.moveToTop = options.moveToTop;
        this.confirmed = options.confirmed;
        
        if (options && options.allowSkip != undefined) {
            this.allowSkip = options.allowSkip;
        } else {
            this.allowSkip = true;
        }
    }

    sortRuleOutRules() {
        let ruleOutRules = this.ruleOutRules;
        this.ruleOutRules = [];
        ruleOutRules.sort(this.compareRules);
        ruleOutRules.forEach(r => this.ruleOutRules.push(r));
        // this.checkForDuplicateRules(this.ruleOutRules);
    }

    sortRules() {
        let rules = this.rules;
        this.rules = [];
        let defaultRule;

        rules.sort(this.compareRules);

        for (let ruleIndex in rules) {
            let rule = rules[ruleIndex];
            if (rule.logic === "default") {
                defaultRule = rule;
            } else {
                this.addRule(rule);
            }
        }

        if (defaultRule) {
            this.addRule(defaultRule);
        }

        // this.checkForDuplicateRules(this.rules);
    }

    compareRules(a, b) {
        let aRuleOrder = 9999999999;
        let bRuleOrder = 9999999999;
        if (a.ruleOrder || a.ruleOrder == 0) {
            aRuleOrder = parseInt(a.ruleOrder);
        }
        if (b.ruleOrder || b.ruleOrder == 0) {
            bRuleOrder = parseInt(b.ruleOrder);
        }
        let primarySortResult = aRuleOrder - bRuleOrder; // Primary-Sort in an increasing order of ruleOrder

        if (primarySortResult && primarySortResult != 0)
            return primarySortResult;

        // Secondary-Sort in a decending order of subrules - The more complex the rule is, the earlier it will be evaluated.
        let aSubrules = 0;
        let bSubrules = 0;
        if (a.subRules) {
            aSubrules = a.subRules.length;
        }
        if (b.subRules) {
            bSubrules = b.subRules.length;
        }

        // TODO: Should we look into "or" or "and"? "and" is more complex than "or"
        return bSubrules - aSubrules;
    }

    /**
     * This is only for debugging & finding doubles in the logic
     */
    checkForDuplicateRules(rules) {
        let answersToNext = {};
        for (let ruleIndex in rules) {
            let rule = rules[ruleIndex];
            let subRules = rule.subRules;
            if (subRules) {
                for (let subruleIndex in subRules) {
                    let subRule = subRules[subruleIndex];
                    let condition = subRule.condition;
                    if (condition) {
                        let key = condition.questionId+"_"+condition.answer;
                        if (answersToNext[key]) {
                            answersToNext[key].push(rule.nextId);
                        } else {
                            answersToNext[key] = [rule.nextId];
                        }
                    }
                }
            }
        }

        for (let key in answersToNext) {
            let next = answersToNext[key];
            if (next.length > 1) {
                let nextIds = "";
                for (let index in next) {
                    nextIds += next[index]+" ";
                }
                let answerText = key.substr(this.id.length+1);
                console.log("Found duplicate rule on question: "+this.questionStr+" (questionId " + this.id + "), going to nextQuestionId " + nextIds + "when answering " + answerText);
            }
        }
    }

    addRule(rule) {
        this.rules.push(rule);
    }

    addRuleOutRule(ruleOutRule) {
        this.ruleOutRules.push(ruleOutRule);
    }

    clearTraversedSideChainsWithHistory(visitedQuestions) {
        this.rules.forEach(r => {
            if (r.type == "side-chain" && r.alreadyTraversed && (visitedQuestions && !visitedQuestions.includes(r.nextId) || !visitedQuestions)) {
                r.alreadyTraversed = false;
            }
        });
    }

    clearTraversedSideChains() {
        return this.clearTraversedSideChainsWithHistory(null);
    }

    addAnswers(answers) {
        this.answers = answers;
    }

    nextQuestion(nextQuestion) {
        this.defaultNextId = nextQuestion.id;
    }

    createNodeForGraph() {
        let answers = this.answers.map(answer => {
            return answer.str;
        })
        return {text: this.questionStr, key:this.id, answers:JSON.stringify(answers), allowSkip:this.allowSkip, mode: this.mode, color:"#B2DFDB"}
    };

    createLinksForGraph() {
        let links = [];
        this.answers && this.answers.forEach(answer => {
            if (answer.nextId) {
                links.push({ from: this.id, to: answer.nextId, color: "#5E35B1", logic: "and", subRules:JSON.stringify([{condition: {questionId:this.id, answer:answer.str}}])});
            }
        })
        this.rules && this.rules.forEach(rule => {
            if (!rule.nextId) {
                throw "no nextId on rule for questionId: " + this.id;
            }

            links.push({ from: this.id, to: rule.nextId, color: "#5E35B1", logic: rule.logic, subRules:JSON.stringify(rule.subRules)});
        });

        // TODO: This should be thrown only if the rules don't cover all the options
        // if (!this.defaultNextId && (this.options && this.rules.size < this.options.size)) {
        if (!this.defaultNextId && (this.options && this.rules.length < this.options.length)) {
            throw "no defaultNextId on questionId: " + this.id;
        }

        links.push({ from: this.id, to: this.defaultNextId, color: "#000000", logic: "default"});
        return links;
    };
}

module.exports = Question;
let locX = 50;
let locY = 0;
let deltaX = 100;
let deltaY = 40;

function createGraph(questions) {
    return {class: "GraphLinksModel", nodeDataArray: createGraphNodes(questions), linkDataArray: createGraphLinks(questions)};
}

function createGraphNodes(questions) {
    let nodes = [];
    questions.forEach(question => {
        let node = question.createNodeForGraph();
        node.loc =  locX + " " + locY;
        nodes.push(node);
        updateLocation();
    });
    return nodes;
}

function createGraphLinks(questions) {
    let links = [];
    questions.forEach(question => {
        pushAll(question.createLinksForGraph(), links);
    });
    return links;
}

function pushAll(links, arr) {
    links.forEach(link => {
        arr.push(link);
    })
}

function updateLocation() {
    locY += deltaY;
    deltaX *= -1;
    locX += deltaX; 
}

module.exports = {createGraph};
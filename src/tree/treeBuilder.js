const Question = require('../question/question');
// const consts = require('../consts');
const RuleOutProbability = require('../summary/RuleOutProbability');

let currentId = 1;
let questions = {};
let ruleOuts = {}; // This should be called "actions" - both ruleOuts and actions
// const GRAPH_PATH = "./data/graph.json";
let inputs = {};

function createQuestions(json) {
    let allQuestions = {};
    let allRuleOuts = {};
    let allInputs = {};

    // Add the questions
    for (let questionIndex in Object.values(json["nodeDataArray"])) {
        let questionData = json["nodeDataArray"][questionIndex];
        questionData.name = questionData.key;
        if (questionData.mode === 'rule-out' || questionData.mode === 'action' || questionData.mode === 'alert') {
            let ruleOut = createQuestion(questionData.text, questionData);
            allRuleOuts[String(questionData.key)] = ruleOut;
        }
        else if (questionData.mode === 'input' || questionData.mode === 'input') {
            // TODO: set type=questionData.inputType for every answer?
            let input = createQuestion(questionData.text, questionData);
            allInputs[String(questionData.key)] = input;
        }
        else {
            let question = createQuestion(questionData.text, questionData);
            allQuestions[String(questionData.key)] = question;
        }
    }

    // Add the rules
    for (let ruleIndex in Object.values(json["linkDataArray"])) {
        let ruleData = json["linkDataArray"][ruleIndex];
        let fromKey = String(ruleData.from);
        let fromQuestion = allQuestions[fromKey];
        let toKey = String(ruleData.to);
        let toQuestion = allQuestions[toKey];
        if (!fromKey || !toKey)
            continue;

        if (!fromQuestion && !allRuleOuts[fromKey] && !allInputs[fromKey]) // If not a valid rule from a question or an action
            continue;

        if (!fromQuestion || !toQuestion) { // Not leading to or coming from a question (R/O or action etc)
            if (!fromQuestion) { // Rule outs
                fromQuestion = allRuleOuts[fromKey];
            }
            if (!toQuestion) {
                toQuestion = allRuleOuts[toKey];
            }

            if (!fromQuestion) { // Inputs
                fromQuestion = allInputs[fromKey];
            }
            if (!toQuestion) {
                toQuestion = allInputs[toKey];
            }
        
            if (fromQuestion && toQuestion) {
                let displayReason = true;
                if (ruleData.display == false) {
                    displayReason = false;
                }
                fromQuestion.addRuleOutRule({logic: ruleData.logic, nextId: toKey, subRules: ruleData.subRules, ruleOrder: ruleData.order, strength: getProbability(ruleData.strength), displayReason: displayReason});
            }
            continue;
        }
        else {
            fromQuestion.addRule({logic: ruleData.logic, nextId: toKey, subRules: ruleData.subRules, type: ruleData.type, ruleOrder: ruleData.order});
            if (ruleData.logic === "default") {
                fromQuestion.nextQuestion(toKey);
            }
        }
    }





    // // TODO: Maybe Go over all the subrules, and for each subrule that references a question, add this ruleOutRule to that question as well.
    // for (let questionKey in allQuestions) {
    //     if (questionKey.rules)
    // }





    for (let key in allQuestions) {
        let question = allQuestions[key];
        // TODO: Remove stand-alone questions

        // if (question && (!question.defaultNextId || !question.nextId)) {
        //     delete allQuestions[key];
        // } 
        // else
        if (question) { // Sort the rules for each question
            question.sortRules();
            question.sortRuleOutRules();
        }
    }

    questions = allQuestions;
    ruleOuts = allRuleOuts;
    inputs = allInputs;
    return questions;
}

function getProbability(text) {
    switch (text) {
            case "מחליש מאוד":
                return RuleOutProbability.RuleOutProbability.VERY_WEAK;
            case "מחליש":
                return RuleOutProbability.RuleOutProbability.WEAK;
            case "ניטרלי":
                return RuleOutProbability.RuleOutProbability.NEUTRAL;
            case "מחזק":
                return RuleOutProbability.RuleOutProbability.STRONG;
            case "מחזק מאוד":
                return RuleOutProbability.RuleOutProbability.VERY_STRONG;
            case "must":
                return RuleOutProbability.RuleOutProbability.MUST;
    }
}

function createQuestion(text, options) {
    if (options.name) {
        let question = new Question(String(options.name), text, options);
        questions[options.name] = question;
        return question;
    }

    let question = new Question(String(currentId), text, options);
    questions[currentId] = question;
    currentId++;
    return question;
}

function getRuleOutMap() {
    return ruleOuts;
}

function getInputMap() {
    return inputs;
}

module.exports = {createQuestions, getRuleOutMap, getInputMap};
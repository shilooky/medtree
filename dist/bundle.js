/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/actionEvaluator.js":
/*!********************************!*\
  !*** ./src/actionEvaluator.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let ruleEvaluator = __webpack_require__(/*! ./ruleEvaluator */ "./src/ruleEvaluator.js")

function evaluateActions(question) {
    if (!question.ruleOutRules) 
        return;

    let appliedRuleOutIds = [];
    for (let ruleOutRuleIndex in question.ruleOutRules) {
        let ruleOutRule = question.ruleOutRules[ruleOutRuleIndex];
        let isRuleTrue = ruleEvaluator.isRuleTrue(ruleOutRule);

        if (isRuleTrue) {
            let actualTrueRules = getTrueRules(ruleOutRule);
            appliedRuleOutIds.push({isApplied: isRuleTrue, ruleOutId: ruleOutRule.nextId, reasons: actualTrueRules, displayReason: ruleOutRule.displayReason});
        }

        // TODO: ? this causes a lot of mess..
        // // Even if the GLOBAL rule doesn't match, we add the SUBrules that do match
        // let actualTrueRules = getTrueRules(ruleOutRule);
        // appliedRuleOutIds.push({isApplied: isRuleTrue, ruleOutId: ruleOutRule.nextId, reasons: actualTrueRules});
    }
    return appliedRuleOutIds;
}

// function getTrueRules(ruleOutRule) {
//     if (!ruleOutRule.subRules)
//         return [ruleOutRule];

//     let trueRules = [];
//     let setStrengthOnSubrule = false;
//     for (let index in ruleOutRule.subRules) {
//         let r = ruleOutRule.subRules[index];
//         if (ruleEvaluator.isRuleTrue(r)) {
//             trueRules.push(r);

//             if (!setStrengthOnSubrule && !r.strength) { // We only want to count it once.
//                 setStrengthOnSubrule = true;
//                 r.strength = ruleOutRule.strength;
//             } else if (!setStrengthOnSubrule && ruleOutRule.strength) {
//                 console.log("This is probly a bug where we set the strength on a subrule that has been set before (from a different question i guess?). potentially can be only 1 subrule, and then we miss a strength count");
//             }

//             // TODO: "or" should show everything?
//             // if (ruleOutRule.logic === "or") // This is "or" - we only need one.
//             //     break;
//         }
//     }

//     return trueRules;
// }

function getTrueRules(ruleOutRule) {
    if (!ruleOutRule.subRules)
        return [ruleOutRule];

    let trueRules = [];
    let setStrengthOnSubrule = false;
    for (let index in ruleOutRule.subRules) {
        let r = ruleOutRule.subRules[index];
        if (r.subRules) {
            let trueSubrules = getTrueRules(r);
            trueSubrules.forEach(r => {
                r.displayReason = ruleOutRule.displayReason;
                trueRules.push(r);
            });
        } else {
            if (ruleEvaluator.isRuleTrue(r)) {
                r.displayReason = ruleOutRule.displayReason;
                trueRules.push(r);

                if (!setStrengthOnSubrule && (!r.strength && r.strength!=0)) { // We only want to count it once.
                    setStrengthOnSubrule = true;
                    r.strength = ruleOutRule.strength;
                } else if (!setStrengthOnSubrule && (ruleOutRule.strength)) {
                    console.log("This is probly a bug where we set the strength on a subrule that has been set before (from a different question i guess?). potentially can be only 1 subrule, and then we miss a strength count");
                }

                // TODO: "or" should show everything?
                // if (ruleOutRule.logic === "or") // This is "or" - we only need one.
                //     break;
            }
        }
    }
    return trueRules;
}

module.exports = {evaluateActions}

/***/ }),

/***/ "./src/angular/angularFileLoader.js":
/*!******************************************!*\
  !*** ./src/angular/angularFileLoader.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../question/questionController */ "./src/question/questionController.js");
__webpack_require__(/*! ../question/questionDirective */ "./src/question/questionDirective.js");
__webpack_require__(/*! ../gate/gateController */ "./src/gate/gateController.js");
__webpack_require__(/*! ../gate/gateDirective */ "./src/gate/gateDirective.js");

/***/ }),

/***/ "./src/angular/angularModule.js":
/*!**************************************!*\
  !*** ./src/angular/angularModule.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const angularModule = angular.module('app', []);

module.exports = angularModule;


/***/ }),

/***/ "./src/consts.js":
/*!***********************!*\
  !*** ./src/consts.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

consts = {
    questionMode: {
        BUTTONS: "buttons",
        FREE_TEXT: "free-text",
        MULTI_CHOICE: "multi-answer"
    }
}

module.exports = consts;

/***/ }),

/***/ "./src/gate/gate.html":
/*!****************************!*\
  !*** ./src/gate/gate.html ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"gate container\">\r\n    <div class=\"gate-wrapper\" ng-if=\"!clickedNext\">\r\n        <div class=\"gate-messege row\">\r\n            <h1 class=\"display-1\"><b>SmartED</b></h1>\r\n        </div>\r\n        <div class=\"gate-button d-flex justify-content-around\">\r\n            <button class=\"btn btn-primary btn-xlarge\" ng-click=\"next()\">המשך</button>\r\n        </div>\r\n        <!-- <div>\r\n            <span class=\"inc-span\"><b>.Smart Medical Inc</b></span>\r\n        </div> -->\r\n    </div>\r\n    \r\n    <question ng-if=clickedNext></question>\r\n    \r\n</div>";

/***/ }),

/***/ "./src/gate/gateController.js":
/*!************************************!*\
  !*** ./src/gate/gateController.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const angularModule = __webpack_require__(/*! ../angular/angularModule */ "./src/angular/angularModule.js");
angularModule.controller('gateController', gateController);
let tree = __webpack_require__(/*! ../tree/tree */ "./src/tree/tree.js");

function gateController($scope) {
    function ctor(){
        $scope.next = next;
    }

    function next() {
        if (!tree.getAllQuestions()) {
            alert('Please upload a file first.');
        } else {
            $scope.clickedNext = true;
        }
    }

    ctor();
}



/***/ }),

/***/ "./src/gate/gateDirective.js":
/*!***********************************!*\
  !*** ./src/gate/gateDirective.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const gateTemplate = __webpack_require__(/*! ./gate.html */ "./src/gate/gate.html");
const angularModule = __webpack_require__(/*! ../angular/angularModule */ "./src/angular/angularModule.js");
angularModule.directive('gate', gateDirective);

function gateDirective() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        template: gateTemplate,
        controller: 'gateController',
        controllerAs: 'gateController'
    }
}

/***/ }),

/***/ "./src/graph/graphCreator.js":
/*!***********************************!*\
  !*** ./src/graph/graphCreator.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

let locX = 50;
let locY = 0;
let deltaX = 100;
let deltaY = 40;

function createGraph(questions) {
    return {class: "GraphLinksModel", nodeDataArray: createGraphNodes(questions), linkDataArray: createGraphLinks(questions)};
}

function createGraphNodes(questions) {
    let nodes = [];
    questions.forEach(question => {
        let node = question.createNodeForGraph();
        node.loc =  locX + " " + locY;
        nodes.push(node);
        updateLocation();
    });
    return nodes;
}

function createGraphLinks(questions) {
    let links = [];
    questions.forEach(question => {
        pushAll(question.createLinksForGraph(), links);
    });
    return links;
}

function pushAll(links, arr) {
    links.forEach(link => {
        arr.push(link);
    })
}

function updateLocation() {
    locY += deltaY;
    deltaX *= -1;
    locX += deltaX; 
}

module.exports = {createGraph};

/***/ }),

/***/ "./src/inputs/inputState.js":
/*!**********************************!*\
  !*** ./src/inputs/inputState.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const angularModule = __webpack_require__(/*! ../angular/angularModule */ "./src/angular/angularModule.js");

let state = {};

function get() {
    return state;
}

module.exports = {get}

/***/ }),

/***/ "./src/main.js":
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let tree = __webpack_require__(/*! ./tree/tree */ "./src/tree/tree.js");

function init() {
    tree.init();
}

init();

__webpack_require__(/*! ./angular/angularModule */ "./src/angular/angularModule.js");
__webpack_require__(/*! ./angular/angularFileLoader */ "./src/angular/angularFileLoader.js");

/***/ }),

/***/ "./src/question/question.html":
/*!************************************!*\
  !*** ./src/question/question.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <div class=\"buttons-panel\">\r\n        <button class=\"skip btn btn btn-secondary btn-sm\" ng-if=\"visitedQuestions.length>1\" ng-click=\"back()\">Back</button>\r\n        <button class=\"skip btn btn btn-secondary btn-sm\" ng-click=\"showSettingsDialog()\">Settings</button>\r\n        <button class=\"skip btn btn btn-secondary btn-sm\" ng-click=\"showInputsDialog()\">Inputs</button>\r\n        <button class=\"skip btn btn btn-secondary btn-sm\" ng-click=\"showSummary()\">Summary</button>\r\n        <button class=\"skip btn btn btn-secondary btn-sm\">Statistics</button>\r\n    </div>\r\n\r\n    <h1 class=\"question display-1\">{{current.questionStr}}</h1>\r\n    <h4 class=\"question-subtitle display-4\">{{current.subTitle}}</h4>\r\n\r\n    <div class=\"answers row d-flex justify-content-around\" ng-if=\"current.mode==questionMode.BUTTONS\">\r\n        <span ng-repeat=\"answer in current.answers\">\r\n            <button ng-if=\"answer.selected\" class=\"answer btn btn-primary btn-xlarge selected\" ng-click=\"next(answer)\">{{answer.str}}</button>\r\n            <button ng-if=\"!answer.selected\" class=\"answer btn btn-primary btn-xlarge\" ng-click=\"next(answer)\">{{answer.str}}</button>\r\n        </span>\r\n    </div>\r\n\r\n    <div class=\"answers row d-flex justify-content-around\" ng-if=\"current.mode==questionMode.MULTI_CHOICE\">\r\n        <span ng-if=\"current.multiChoiceYesNo\">\r\n            <span ng-repeat=\"answer in current.answers\" class=\"yesNoAnswers\">\r\n                <br/>\r\n                <a class=\"yes-no-answer-text\">{{answer.str}}</a>\r\n                <span class=\"yes-no-buttons-span\">\r\n                    <button ng-if=\"!answer.isAnswered || !answer.selected\" class=\"answer btn btn-primary btn-xlarge\" ng-click=\"setSelectionOnYesNo(answer, true)\">כן</button>\r\n                    <button ng-if=\"answer.selected\" class=\"answer btn btn-primary btn-xlarge selected\">כן</button>\r\n                    <button ng-if=\"!answer.isAnswered || answer.selected\" class=\"answer btn btn-primary btn-xlarge\" ng-click=\"setSelectionOnYesNo(answer, false)\">לא</button>\r\n                    <button ng-if=\"answer.isAnswered && !answer.selected\" class=\"answer btn btn-primary btn-xlarge selected\">לא</button>\r\n                </span>\r\n                <br/>\r\n            </span>\r\n            <br/>\r\n        </span>\r\n        <span ng-if=\"!current.multiChoiceYesNo\" class=\"multiAnswers\">\r\n            <span ng-repeat=\"answer in current.answers\" class=\"multi-answer-buttons-span\">\r\n                <button ng-if=\"answer.selected\" class=\"answer btn btn-primary btn-xlarge selected\" ng-click=\"toggleSelection(answer)\">{{answer.str}}</button>\r\n                <button ng-if=\"!answer.selected\" class=\"answer btn btn-primary btn-xlarge\" ng-click=\"toggleSelection(answer)\">{{answer.str}}</button>\r\n            </span>\r\n        </span>\r\n        <div class=\"d-flex justify-content-around nextDiv\"> \r\n            <button class=\"answer btn btn-primary btn-xlarge continue\" ng-disabled=\"isNextDisabled()\" ng-click=\"next({str:freeTextAnswer})\">המשך</button>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"free-text-wrapper\" ng-if=\"current.mode==questionMode.FREE_TEXT\">\r\n            <div class=\"d-flex justify-content-around\"> \r\n                <textarea ng-model=\"freeTextAnswer\"></textarea>\r\n            </div>\r\n            <div class=\"d-flex justify-content-around\"> \r\n                <button class=\"answer btn btn-primary btn-xlarge\" ng-click=\"next({str:freeTextAnswer})\">המשך</button>\r\n            </div>\r\n    </div>\r\n\r\n    <div class=\"skip-wraper d-flex justify-content-around\" ng-if=\"current.allowSkip\"> \r\n        <button class=\"skip btn btn btn-secondary btn-sm\" ng-click=\"next({str:'לא יודע'})\" ng-show=\"current.allowSkip\">לא יודע \\ דלג</button>\r\n    </div>\r\n    \r\n    <!-- <div class=\"print-summary-wraper d-flex justify-content-around\">\r\n        <button class=\"btn btn btn-secondary btn-sm\" ng-click=\"printSummary()\" ng-show=\"end && !summary\">הדפס סיכום</button>\r\n    </div>\r\n    <div class=\"summary-wraper d-flex justify-content-around\"> \r\n        <h5 class=\"summary-print display-5\" ng-show=\"summary && end\">{{summary}}</h1>\r\n    </div> -->\r\n</div>\r\n\r\n<div class=\"popup\" ng-click=\"hideRuleOutsPopup(true)\">\r\n    <span class=\"popuptext ruleout\" id=\"ruleOutsPopup\">{{this.getRuleOutsPopupText()}}</span>\r\n</div>\r\n\r\n<div class=\"popup-action\" ng-click=\"hideActionsPopup(true)\">\r\n    <span class=\"popuptext-action\" id=\"actionsPopup\">{{this.getActionsPopupText()}}</span>\r\n</div>\r\n\r\n<div id=\"myModal\" class=\"modal\" ng-style=\"{overflow:'auto'}\">\r\n    <div class=\"modal-content\">\r\n        <span class=\"myModal-close\">&times;</span>\r\n        <span class=\"modal-text\">\r\n            <div class=\"alerts\" ng-if=\"summary.alerts && summary.alerts.length>0\">\r\n                <h3>התרעות</h3><span class=\"summary-alert\" ng-repeat=\"alert in summary.alerts\" ng-click=\"showReasons(alert)\">{{alert.name}}<br/></span>\r\n            </div>\r\n            <div>\r\n                <h3>אבחנה מבדלת</h3>\r\n                <div class=\"confirmed-rule-outs\" ng-repeat=\"ruleOut in summary.confirmedRuleOuts\" ng-click=\"showReasons(ruleOut)\">\r\n                    {{ruleOut.name}}<br/>\r\n                 </div>\r\n                <div class=\"top-rule-outs\" ng-repeat=\"ruleOut in summary.topRuleOuts\" ng-click=\"showReasons(ruleOut)\">\r\n                   {{ruleOut.name}}<br/>\r\n                </div>\r\n                <div class=\"summary-rule-out\" ng-repeat=\"ruleOut in summary.ruleOuts\" ng-click=\"showReasons(ruleOut)\">\r\n                    <b ng-if=\"ruleOut.strengthText\" ng-class=\"getStrengthTextClass(ruleOut)\">{{ruleOut.strengthText}}: </b>{{ruleOut.name}}<br/>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"summary-section\">\r\n                <h3>אנמנזה</h3><br/>\r\n                <span class=\"summary-text\" ng-repeat=\"paragraph in summary.anamnesisStory\">\r\n                    <span ng-if=\"hasRows(paragraph)\"><b>{{paragraph.title}}</b><br/>\r\n                        <span ng-repeat=\"row in paragraph.rows\"><label ng-if=\"row\">{{row}}</label></span><br/>\r\n                    </span>\r\n                    <!-- <br ng-if=\"hasRows(paragraph)\"/> -->\r\n                </span>\r\n            </div>\r\n\r\n            <div class=\"summary-section\">\r\n                <h3 class=\"summary-link\" ng-click=\"showAnswersSummary()\"><a>פירוט תשובות</a></h3>\r\n            </div>\r\n            <hr>\r\n            <div class=\"summary-section\">\r\n                <h3>אפשרויות להמשך בירור</h3>\r\n                <div ng-repeat=\"actionSection in summary.actionSections\" ng-if=\"actionSection.actions.length > 0\">\r\n                    <b>{{actionSection.name}}:</b><br>\r\n                    <span class=\"summary-action\" ng-repeat=\"action in actionSection.actions\">\r\n                        <a>{{action.name}}</a> \r\n                        <span ng-style=\"{'font-size':'x-large','color':'green'}\" ng-if=\"shouldDisplayActionConfirmButton(action) && action.isConfirmed\"><b>✓</b></span>\r\n                        <button class=\"answer btn btn-primary btn-small\" ng-if=\"shouldDisplayActionConfirmButton(action) && !action.isConfirmed\" ng-click=\"performAction(action)\">בצע</button><br/>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </span>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"item-modal\" class=\"modal\" ng-style=\"{overflow:'auto'}\">\r\n    <div class=\"item-modal-content\">\r\n        <span class=\"item-modal-close\">&times;</span>\r\n        <br/>\r\n        <br/>\r\n        <h2>{{getItemName()}}</h2>\r\n        <br/>\r\n        <h4>תשובות רלוונטיות</h4>\r\n        <span class=\"modal-text\" ng-repeat=\"reason in getItemData()\"><b>{{reason.questionStr}}:</b> {{reason.answerStr}}<br/></span>\r\n        <span ng-if=\"!(data=getItemData())\">Not Available</span>\r\n        <div class=\"modal-text rule-out-summary\" ng-if=\"shouldShowRuleOutSummary()\"><br/><br/><b>{{getRuleOutSummary()}}</b></div>\r\n        <span ng-if=\"getItemScore() && (getItemScore().value || getItemScore().value==0)\" class=\"score-span\">\r\n            <br/>\r\n            <br/>\r\n            <h4 class=\"score-title\" ng-if=\"getItemScore().name\">{{getItemScore().name}}</h4>\r\n            <h4 class=\"score-title\" ng-if=\"getItemScore().displayScore\">Score: {{getItemScore().value}}</h4>\r\n            <span class=\"modal-text\" ng-if=\"getItemScore().reasons && getItemScore().reasons.length>0\" ng-repeat=\"reason in getItemScore().reasons\"><b>{{reason.questionStr}}<span ng-if=\"reason.answerStr && reason.answerStr.length>0\">:</span></b><span ng-if=\"reason.answerStr && reason.answerStr.length>0\"> {{reason.answerStr}}</span><b ng-if=\"reason.scoreAmount && reason.scoreAmount!=0\"> (<span ng-if=\"reason.scoreAmount>0\" class=\"positive-score-amount\">+{{reason.scoreAmount}}</span><span ng-if=\"reason.scoreAmount<0\" class=\"negative-score-amount\">{{reason.scoreAmount}}</span>)</b><br/></span>\r\n            <br/>\r\n            <span class=\"score-reference-panel\" ng-if=\"getItemScore().reference && getItemScore().reference.trim().length>0\"><b>מקרא:</b><br/>{{getItemScore().reference.trim()}}</span>\r\n        </span>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"answers-summary-modal\" class=\"modal\" ng-style=\"{overflow:'auto'}\">\r\n    <div class=\"answers-summary-modal-content\">\r\n        <span class=\"answers-summary-modal-close\">&times;</span>\r\n        <br/>\r\n        <br/>\r\n        <h2>פירוט תשובות</h2><br/>\r\n        <span ng-repeat=\"q in getAnswersSummary()\" ng-class=\"q.answers? 'summary-rule-out' : ''\" ng-click=\"q.answers && showMultiAnswerSummary(q)\" ng-if=\"q.shouldDisplayForDoctor\">\r\n            <!-- <b>{{q.questionStr}}:</b> {{q.answerStr}}<br/> -->\r\n\r\n            <b>{{q.doctorDisplayText || q.questionStr}}:</b>\r\n            <span ng-if=\"q.answers\" ng-repeat=\"answer in q.answers\">{{answer}}<span ng-if=\"!$last\">, </span></span>\r\n            <span ng-if=\"q.answer\">{{q.answer}}</span>\r\n            <br/>\r\n        </span>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"multi-answer-summary-modal\" class=\"modal\" ng-style=\"{overflow:'auto'}\">\r\n    <div class=\"answers-summary-modal-content\">\r\n        <span class=\"multi-answer-summary-modal-close\">&times;</span>\r\n        <h2>{{selectedMultiAnswerQuestion.questionStr}}</h2><br/>\r\n        <span ng-repeat=\"answer in selectedMultiAnswerQuestion.answers\">\r\n            <span ng-style=\"{'font-size':'larger', 'color':'green'}\">{{answer}} <b>✓</b></span>\r\n            <br/>\r\n        </span>\r\n        <span ng-repeat=\"answer in selectedMultiAnswerQuestion.unselectedAnswers\">\r\n            <span ng-style=\"{'color':'red'}\">{{answer}} <b>✗</b></span>\r\n            <br/>\r\n        </span>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"inputs-modal\" class=\"modal\" ng-style=\"{overflow:'auto'}\">\r\n    <div class=\"inputs-modal-content\">\r\n        <span class=\"inputs-modal-close\">&times;</span>\r\n        <span class=\"modal-text\" ng-repeat=\"inputParameterGroup in getInputParameters()\">\r\n            <b>{{inputParameterGroup.questionStr}}</b><br/>\r\n            <span ng-if=\"true\" ng-repeat=\"parameter in inputParameterGroup.answers\">\r\n                <span ng-if=\"parameter.type != 'radio'\">{{parameter.str}}: </span>\r\n                <span ng-if=\"parameter.type == 'radio'\">{{getRadioTitle(parameter.str)}}:  </span>\r\n                <textarea ng-if=\"parameter.type==='free-text'\" ng-model=\"inputParameters[inputParameterGroup.questionStr+'@@@_@@@'+parameter.str]\"></textarea>\r\n                <input type=\"checkbox\" ng-if=\"parameter.type==='checkbox'\" ng-model=\"inputParameters[inputParameterGroup.questionStr+'@@@_@@@'+parameter.str]\"></input>\r\n                <div class=\"input-radio-option\" ng-if=\"parameter.type==='radio'\" ng-repeat=\"radioValue in getRadioValues(parameter.str)\">\r\n                    <input type=\"radio\" ng-model=\"inputParameters[inputParameterGroup.questionStr+'@@@_@@@'+parameter.str]\" ng-value=\"radioValue\" ng-name=\"parameter.str\"> {{radioValue}}</input>&nbsp;&nbsp;</span><br/>\r\n                </div><br/><br/>\r\n            </span>\r\n        <div class=\"d-flex justify-content-around\">\r\n            <button class=\"answer btn btn-primary btn-xlarge\" ng-click=\"onPressedSubmitInputDialog()\">אישור</button>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"settings-modal\" class=\"modal\" ng-style=\"{overflow:'auto'}\">\r\n    <div class=\"settings-modal-content\">\r\n        <span class=\"settings-modal-close\">&times;</span>\r\n        <br/>\r\n        <h1>אפשרויות</h1>\r\n        <span ng-repeat=\"s in getAppSettings()\">\r\n            <span ng-click=\"s.onChanged()\">{{s.name}} <input type=\"checkbox\" ng-model=\"s.value\"></input><br/></span>\r\n        </span>\r\n        <br/>\r\n        <br/>\r\n        <span class=\"language-span\">\r\n            <b>שפה:</b><br/><input type=\"radio\" id=\"English\" name=\"language\" value=\"English\"> English &nbsp;&nbsp;<input type=\"radio\" checked id=\"Hebrew\" name=\"language\" value=\"Hebrew\"> עברית &nbsp;&nbsp;<input type=\"radio\" id=\"Arabic\" name=\"language\" value=\"Arabic\"> عربى &nbsp;&nbsp;<input type=\"radio\" id=\"Russian\" name=\"language\" value=\"Russian\"> русский &nbsp;&nbsp;\r\n        </span>\r\n        <br/>\r\n\r\n        <br/>\r\n        <div class=\"d-flex justify-content-around\">\r\n            <button class=\"answer btn btn-primary btn-medium\" ng-click=\"closeModal('settings-modal')\">אישור</button>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),

/***/ "./src/question/question.js":
/*!**********************************!*\
  !*** ./src/question/question.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const consts = __webpack_require__(/*! ../consts */ "./src/consts.js");

class Question {
    constructor(id, questionStr, options) {
        this.id = id;
        this.questionStr = questionStr;
        this.subTitle = options.subTitle;

        this.defaultNextId = options.defaultNextId;
        this.answers = (options && options.answers) || [];
        this.rules = options.rules || [];
        this.ruleOutRules = options.ruleOutRules || []; // This should be called "actions" - both ruleOuts and actions
        this.mode = options.mode || consts.questionMode.BUTTONS;
        this.end = options.end;
        this.score = options.score;
        this.multiChoiceYesNo = options.multiChoiceYesNo;
        this.allowSkip = options.allowSkip;
        this.anamnesis = options.anamnesis;
        this.message = options.message;
        this.shouldDisplayForDoctor = options.shouldDisplayForDoctor;
        this.doctorDisplayText = options.doctorDisplayText;
        this.alert = options.alert;
        this.ruleOutText = options.ruleOutText;
        this.actionOrder = options.actionOrder;
        this.actionSection = options.actionSection;
        this.isFirst = options.isFirst;
        this.moveToTop = options.moveToTop;
        this.confirmed = options.confirmed;
        
        if (options && options.allowSkip != undefined) {
            this.allowSkip = options.allowSkip;
        } else {
            this.allowSkip = true;
        }
    }

    sortRuleOutRules() {
        let ruleOutRules = this.ruleOutRules;
        this.ruleOutRules = [];
        ruleOutRules.sort(this.compareRules);
        ruleOutRules.forEach(r => this.ruleOutRules.push(r));
        // this.checkForDuplicateRules(this.ruleOutRules);
    }

    sortRules() {
        let rules = this.rules;
        this.rules = [];
        let defaultRule;

        rules.sort(this.compareRules);

        for (let ruleIndex in rules) {
            let rule = rules[ruleIndex];
            if (rule.logic === "default") {
                defaultRule = rule;
            } else {
                this.addRule(rule);
            }
        }

        if (defaultRule) {
            this.addRule(defaultRule);
        }

        // this.checkForDuplicateRules(this.rules);
    }

    compareRules(a, b) {
        let aRuleOrder = 9999999999;
        let bRuleOrder = 9999999999;
        if (a.ruleOrder || a.ruleOrder == 0) {
            aRuleOrder = parseInt(a.ruleOrder);
        }
        if (b.ruleOrder || b.ruleOrder == 0) {
            bRuleOrder = parseInt(b.ruleOrder);
        }
        let primarySortResult = aRuleOrder - bRuleOrder; // Primary-Sort in an increasing order of ruleOrder

        if (primarySortResult && primarySortResult != 0)
            return primarySortResult;

        // Secondary-Sort in a decending order of subrules - The more complex the rule is, the earlier it will be evaluated.
        let aSubrules = 0;
        let bSubrules = 0;
        if (a.subRules) {
            aSubrules = a.subRules.length;
        }
        if (b.subRules) {
            bSubrules = b.subRules.length;
        }

        // TODO: Should we look into "or" or "and"? "and" is more complex than "or"
        return bSubrules - aSubrules;
    }

    /**
     * This is only for debugging & finding doubles in the logic
     */
    checkForDuplicateRules(rules) {
        let answersToNext = {};
        for (let ruleIndex in rules) {
            let rule = rules[ruleIndex];
            let subRules = rule.subRules;
            if (subRules) {
                for (let subruleIndex in subRules) {
                    let subRule = subRules[subruleIndex];
                    let condition = subRule.condition;
                    if (condition) {
                        let key = condition.questionId+"_"+condition.answer;
                        if (answersToNext[key]) {
                            answersToNext[key].push(rule.nextId);
                        } else {
                            answersToNext[key] = [rule.nextId];
                        }
                    }
                }
            }
        }

        for (let key in answersToNext) {
            let next = answersToNext[key];
            if (next.length > 1) {
                let nextIds = "";
                for (let index in next) {
                    nextIds += next[index]+" ";
                }
                let answerText = key.substr(this.id.length+1);
                console.log("Found duplicate rule on question: "+this.questionStr+" (questionId " + this.id + "), going to nextQuestionId " + nextIds + "when answering " + answerText);
            }
        }
    }

    addRule(rule) {
        this.rules.push(rule);
    }

    addRuleOutRule(ruleOutRule) {
        this.ruleOutRules.push(ruleOutRule);
    }

    clearTraversedSideChainsWithHistory(visitedQuestions) {
        this.rules.forEach(r => {
            if (r.type == "side-chain" && r.alreadyTraversed && (visitedQuestions && !visitedQuestions.includes(r.nextId) || !visitedQuestions)) {
                r.alreadyTraversed = false;
            }
        });
    }

    clearTraversedSideChains() {
        return this.clearTraversedSideChainsWithHistory(null);
    }

    addAnswers(answers) {
        this.answers = answers;
    }

    nextQuestion(nextQuestion) {
        this.defaultNextId = nextQuestion.id;
    }

    createNodeForGraph() {
        let answers = this.answers.map(answer => {
            return answer.str;
        })
        return {text: this.questionStr, key:this.id, answers:JSON.stringify(answers), allowSkip:this.allowSkip, mode: this.mode, color:"#B2DFDB"}
    };

    createLinksForGraph() {
        let links = [];
        this.answers && this.answers.forEach(answer => {
            if (answer.nextId) {
                links.push({ from: this.id, to: answer.nextId, color: "#5E35B1", logic: "and", subRules:JSON.stringify([{condition: {questionId:this.id, answer:answer.str}}])});
            }
        })
        this.rules && this.rules.forEach(rule => {
            if (!rule.nextId) {
                throw "no nextId on rule for questionId: " + this.id;
            }

            links.push({ from: this.id, to: rule.nextId, color: "#5E35B1", logic: rule.logic, subRules:JSON.stringify(rule.subRules)});
        });

        // TODO: This should be thrown only if the rules don't cover all the options
        // if (!this.defaultNextId && (this.options && this.rules.size < this.options.size)) {
        if (!this.defaultNextId && (this.options && this.rules.length < this.options.length)) {
            throw "no defaultNextId on questionId: " + this.id;
        }

        links.push({ from: this.id, to: this.defaultNextId, color: "#000000", logic: "default"});
        return links;
    };
}

module.exports = Question;

/***/ }),

/***/ "./src/question/questionController.js":
/*!********************************************!*\
  !*** ./src/question/questionController.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const angularModule = __webpack_require__(/*! ../angular/angularModule */ "./src/angular/angularModule.js");
const summary = __webpack_require__(/*! ../summary/summary */ "./src/summary/summary.js");
const ruleEvaluator = __webpack_require__(/*! ../ruleEvaluator */ "./src/ruleEvaluator.js");
const actionEvaluator = __webpack_require__(/*! ../actionEvaluator */ "./src/actionEvaluator.js");
let tree = __webpack_require__(/*! ../tree/tree */ "./src/tree/tree.js");
let medicalSummary = __webpack_require__(/*! ../summary/medicalSummary */ "./src/summary/medicalSummary.js");
let consts = __webpack_require__(/*! ../consts */ "./src/consts.js");
angularModule.controller('questionController', questionController);
let sideChainNavigator = __webpack_require__(/*! ./sideChainNavigator */ "./src/question/sideChainNavigator.js");
let answerRetriever = __webpack_require__(/*! ../summary/answerRetriever */ "./src/summary/answerRetriever.js");
const RuleOutProbability = __webpack_require__(/*! ../summary/RuleOutProbability */ "./src/summary/RuleOutProbability.js");
const ruleOutSummary = __webpack_require__(/*! ../summary/ruleOutSummary */ "./src/summary/ruleOutSummary.js");
const inputState = __webpack_require__(/*! ../inputs/inputState */ "./src/inputs/inputState.js");
let settings = __webpack_require__(/*! ../settings */ "./src/settings.js");

let visitedQuestions = [];
function questionController($scope) {
    function ctor(){
        $scope.next = next;
        $scope.toggleSelection = toggleSelection;
        $scope.back = back;
        $scope.current = tree.getFirst();
        $scope.questionMode = consts.questionMode;
        $scope.printSummary = printSummary;
        $scope.showSummary = showSummary;
        $scope.showReasons = showReasons;
        $scope.showAnswersSummary = showAnswersSummary;
        $scope.showMultiAnswerSummary = showMultiAnswerSummary;
        $scope.showInputsDialog = showInputsDialog;
        $scope.showSettingsDialog = showSettingsDialog;
        $scope.getAnswersSummary = getAnswersSummary;
        $scope.hasRows = hasRows;
        $scope.isNextDisabled = isNextDisabled;
        $scope.setSelectionOnYesNo = setSelectionOnYesNo;
        $scope.getInputParameters = getInputParameters;
        $scope.onPressedSubmitInputDialog = onPressedSubmitInputDialog;
        $scope.closeModal = closeModal;
        $scope.getAppSettings = getAppSettings;
        $scope.shouldDisplayActionConfirmButton = shouldDisplayActionConfirmButton;
        $scope.getRadioValues = getRadioValues;
        $scope.getRadioTitle = getRadioTitle;
        $scope.getNgModelName = getNgModelName;
        $scope.performAction = performAction;
        $scope.getRuleOutSummary = getRuleOutSummary;
        $scope.shouldShowRuleOutSummary = shouldShowRuleOutSummary;
        $scope.visitedQuestions = visitedQuestions;
        visitedQuestions.push($scope.current.id);

        // Popups
        $scope.getRuleOutsPopupText = getRuleOutsPopupText;
        $scope.getActionsPopupText = getActionsPopupText;
        $scope.hideRuleOutsPopup = hideRuleOutsPopup;
        $scope.hideActionsPopup = hideActionsPopup;
        $scope.getItemData = getItemData;
        $scope.getItemName = getItemName;
        $scope.getItemScore = getItemScore;
        $scope.getStrengthTextClass = getStrengthTextClass;
        $scope.allQuestionsAnswers = allQuestionsAnswers;

        // Inputs
        $scope.inputParameters = inputState.get();
    }

    function setSelectionOnYesNo(answer, isYes) {
        let oldAnswer = answer.selected;
        if (isYes) {
            answer.selected = true;
        } else {
            answer.selected = false;
        }

        if (isYes != oldAnswer) {
            summary.append($scope.current, answer);
        }
        answer.isAnswered = true;
    }

    function toggleSelection(answer) {
        answer.selected = !answer.selected;
        summary.append($scope.current, answer);
    }

    function selectSingleAnswer(answer) {
        if (!answer || !answer.str)
            return;

        summary.append($scope.current, answer);
        $scope.current.answers.forEach(a => {
            a.selected = false;
        });
        answer.selected = true;
    }

    function getNextQuestionId(question, answer) {
        let firstAppliedRule;
        let nextId;
        if (answer) {
            answer = answer.nextId;
        }
        if (nextId === undefined) {
            firstAppliedRule = ruleEvaluator.getFirstAppliedRule(question);
        }
        if (firstAppliedRule) {
            nextId = firstAppliedRule.nextId;
            if (firstAppliedRule.type == "side-chain") {
                sideChainNavigator.setState(question);
                console.log("Starting side-chain from question: " + question.id + " to question " + nextId);
            }
        } else {
            nextId = $scope.current.defaultNextId;
        }
        return nextId;
    }

    function insertEmptyArrayForEmptyAnswer() {
        let answersMap = summary.getMap();
        if (!answersMap[$scope.current.id]) {
            summary.setEmptyAnswer($scope.current);
        }
    }

    // This function aligns the question.answers array (which holds all the answers, with a "selected" flag) with the answer.selectedQuestions array.
    // (In case we backed up and unselected an answer)
    function clearUnselectedAnswers() {
        let answer = summary.get($scope.current.id);

        let selectedAnswersToRemove = [];
        if (answer && answer.question && answer.question.answers && answer.selectedAnswers) {
            answer.question.answers.forEach(actualAnswer => {
                let selectedAnswer = answer.selectedAnswers.find(a => actualAnswer.str === a.str)
                if (selectedAnswer && !actualAnswer.selected) {
                    console.log("Found an answer in selectedAnswers, that is not selected in the actual question answers.");
                    selectedAnswer.selected = false;
                    selectedAnswersToRemove.push(selectedAnswer);
                }
            });
        }

        while (selectedAnswersToRemove.length>0) {
            let toRemove = selectedAnswersToRemove[0];
            let index = answer.selectedAnswers.indexOf(toRemove);
            if (index != -1) {
                if (toRemove) {
                    console.log("Removing from selectedAnswers: " + toRemove.str);
                }
                answer.selectedAnswers.splice(index, 1);
            }
            selectedAnswersToRemove.splice(0, 1);
        }
    }

    function next(answer) {
        selectSingleAnswer(answer);
        clearUnselectedAnswers();
        insertEmptyArrayForEmptyAnswer();

        let nextId = getNextQuestionId($scope.current, answer);

        // Apply the new state
        applyRuleOuts($scope.current);
        summary.setVisitedNode($scope.current.id, true); // This should be before I guess?

        // Save a snapshot of the state of this question
        if (visitedQuestions.indexOf($scope.current.id) == visitedQuestions.length-1) { // Only save if we haven't been here (if the first (and only) occurence of this question is the last index.) - for double-nexts when we finish side-chains
            let answers = summary.get($scope.current.id);
            if (answers) { // Could be that the user pressed "Skip" or answered an empty free-text area
                if (answers.answer) {
                    answers = answers.answer;
                } else if (answers.selectedAnswers) {
                    answers = [...new Set(answers.selectedAnswers)];
                }

                questionStates[$scope.current.id] = {actions: [...new Set(summary.getRuleOuts())], visitedNodes: [...new Set(summary.getVisitedNodes())], answers: answers};
            }
        }

        let movedToNext = true;
        if (tree.getQuestion(nextId)) {
            $scope.current = tree.getQuestion(nextId);
            $scope.end = $scope.current.end;
            lastAppliedRuleOuts = [];
        } else {
            let sideChainStartQuestion = sideChainNavigator.getState();
            if (!sideChainStartQuestion && $scope.current.baseQuestionId) { // This is for when we back to the end of a side-chain
                sideChainStartQuestion = tree.getQuestion($scope.current.baseQuestionId);
            }

            if (sideChainStartQuestion) {
                $scope.current.baseQuestionId = sideChainStartQuestion.id;
                $scope.current = sideChainStartQuestion;
                $scope.end = $scope.current.end;
                lastAppliedRuleOuts = [];

                console.log("Finished side-chain from question: " + sideChainStartQuestion.id);
                sideChainNavigator.setState(null);
                next(answerRetriever.getAnswer(sideChainStartQuestion.id));
            } else {
                alert("Graph reached a dead end");
                movedToNext = false;
            }
        }
        hideRuleOutsPopup(false);
        hideActionsPopup(false);
        if (movedToNext) {
            if (!visitedQuestions.includes($scope.current.id)) { // If we've been here, this is a side-chain ending. No need to add the base question again.
                visitedQuestions.push($scope.current.id);
            }
        }

        let currentQuestionSnapshot = questionStates[$scope.current.id];
        if (currentQuestionSnapshot) {
            summary.restoreAnswers($scope.current, currentQuestionSnapshot.answers);
        }
    }

    let questionStates = {}; // For every question, a snapshots of the ruleouts in that point. (Stored AFTER answering the questions, so it's inclusive to that question)

    let lastAppliedRuleOuts = [];
    function applyRuleOuts(question) {
        let appliedRuleOuts = evaluateActionsRecursively(question);
        if (!appliedRuleOuts || appliedRuleOuts.length==0)
            return;

        allRuleOutsTexts = "";
        allActionsTexts = "";
        lastAppliedRuleOuts = [];
        let delimiter = "\n";

        // TODO: This is stupid, we do this twice?
        appliedRuleOuts.forEach(ro => {
            let roId = ro.ruleOutId;
            let roReasons = ro.reasons;
            let ruleOut = tree.getRuleOut(roId);
            if (!ruleOut) // TODO: Again, this is a case where an action eventually redirects back to a question
                return;

            if (ruleOut["reasons"]) {
                for (let index in ruleOut.reasons) { // Remove the old reasons from this question (if we went back and changed the answer)
                    let oldReason = ruleOut.reasons[index];
                    if (oldReason.condition && oldReason.condition.questionId == $scope.current.id && visitedQuestions[visitedQuestions.length-1] == oldReason.condition.questionId) { // if the old reason is from this question, and it's the last question we visited (which doesn't happen on double-next (side-chain), but does happen when pressing back and then changing answers)
                        console.log('Removing reason: ' + oldReason.condition.answer);
                        ruleOut.reasons.splice(index, 1);
                        // let roReason = roReasons.find(r => r.condition && r.condition.questionId === oldReason.condition.questionId); // Get the same reason in the roReasons array
                        // if (roReason) {
                        //     let roReasonIndex = roReasons.indexOf(roReason);
                        //     if (roReasonIndex != -1) {
                        //         roReasons.splice(roReasonIndex, 1);
                        //     }
                        // }
                    }
                }
            }
        });

        appliedRuleOuts.forEach(ro => {
            let roId = ro.ruleOutId;
            let roReasons = ro.reasons;
            let ruleOut = tree.getRuleOut(roId);
            if (!ruleOut) // TODO: Again, this is a case where an action eventually redirects back to a question
                return;

            if (ruleOut["reasons"]) {
                roReasons.forEach(reason => ruleOut["reasons"].push(reason));
                checkForSameQuestionDuplicates(ruleOut["reasons"]);
            } else {
                ruleOut["reasons"] = roReasons;
            }
            ruleOut["reasons"] = eliminateDuplicateReasons(ruleOut["reasons"]);
            if (ro.isApplied) { // Only if the rule is true. This is done for when we have a R/O rule: {and: [x,y]} but only x isTrue - we want to add x as a reason, but not to add the ruleout itself, because the GLOBAL rule doesn't apply.
                lastAppliedRuleOuts.push(ruleOut);
            }
        });

        removeNonMustRuleOuts(lastAppliedRuleOuts);

        lastAppliedRuleOuts = [...new Set(lastAppliedRuleOuts)];
        lastAppliedRuleOuts.forEach(ruleOut => {
            // Only if this is the first time adding the action/rule-out
            let previousActions = questionStates[getQuestionBeforePreviousId()]; // The state before answering this question
            if (previousActions && previousActions.visitedNodes && previousActions.visitedNodes.includes(ruleOut.id))
                return;

            if (ruleOut)
            if (ruleOut.mode === "rule-out") {
                if(ruleOut.alert) {
                    ruleOut["type"] = "alert";
                } else {
                    ruleOut["type"] = "rule-out";
                    allRuleOutsTexts += ruleOut.questionStr + delimiter;
                } 
            } 
            else if (ruleOut.mode === "action") {
                ruleOut["type"] = "action";
                let message = ruleOut.message;
                allActionsTexts += message + delimiter;
            }
        });

        if (allRuleOutsTexts != "") {
            allRuleOutsTexts = allRuleOutsTexts.substring(0, allRuleOutsTexts.length-(delimiter.length));
            showRuleOutsPopup();
        }
        if (allActionsTexts != "") {
            let allRows = allActionsTexts.split(delimiter);
            allRows = [...new Set(allRows)];
            allActionsTexts = "";
            allRows.forEach(r => allActionsTexts += r+delimiter);
            allActionsTexts = allActionsTexts.substring(0, allActionsTexts.length-(delimiter.length));
            showActionsPopup();
        }
        // eliminateThisQuestionsLastAppliedRuleOuts(lastAppliedRuleOuts);
        summary.addRuleOuts(lastAppliedRuleOuts);
    }


    function removeRuleOutReasons(ruleOutId, rule) {
        if (rule.condition) {
            let ruleOut = tree.getRuleOut(ruleOutId);
            if (!ruleOut.reasons)
                return; // This rule out isn't applied

            let relevantReason = ruleOut.reasons.find(reason => reason == rule);
            if (relevantReason) {
                let index = ruleOut.reasons.indexOf(relevantReason);
                if (index != -1) {
                    ruleOut.reasons.splice(index, 1);
                }
            }

            if (ruleOut.reasons.length == 0) {
                summary.removeRuleOut(ruleOut);
                summary.setVisitedNode(ruleOutId, false);
            }

        } else if (rule.subRules) {
            rule.subRules.forEach(sr => removeRuleOutReasons(ruleOutId, sr));
        }
    }


    function applyRuleOutsForMultipleQuestions(questions, showPopups) {
        let delimiter = "\n";
        let ruleOutTexts = [];
        let actionsTexts = [];

        for (let key in questions) {
            let question = questions[key];

            // TODO: This should actually happen recursively... what if we remove a R.O that has a default rule to an action? we should remove the action as well

            // First, remove the ruleouts created & reasons by this question. we will re-evaluate it here.
            question.ruleOutRules.forEach(rule => removeRuleOutReasons(rule.nextId, rule));

            let appliedRuleOuts = evaluateActionsRecursively(question);
            if (!appliedRuleOuts || appliedRuleOuts.length==0)
                continue;
    
            // allRuleOutsTexts = "";
            // allActionsTexts = "";
            // lastAppliedRuleOuts = [];
            // let delimiter = "\n";
    
            // TODO: This is stupid, we do this twice?
            appliedRuleOuts.forEach(ro => {
                let roId = ro.ruleOutId;
                let roReasons = ro.reasons;
                let ruleOut = tree.getRuleOut(roId);
                if (!ruleOut) // TODO: Again, this is a case where an action eventually redirects back to a question
                    return;
    
                if (ruleOut["reasons"]) {
                    for (let index in ruleOut.reasons) { // Remove the old reasons from this question (if we went back and changed the answer)
                        let oldReason = ruleOut.reasons[index];
                        if (oldReason.condition && oldReason.condition.questionId == $scope.current.id && visitedQuestions[visitedQuestions.length-1] == oldReason.condition.questionId) { // if the old reason is from this question, and it's the last question we visited (which doesn't happen on double-next (side-chain), but does happen when pressing back and then changing answers)
                            console.log('Removing reason: ' + oldReason.condition.answer);
                            ruleOut.reasons.splice(index, 1);
                        }
                    }
                }
            });
    
            appliedRuleOuts.forEach(ro => {
                let roId = ro.ruleOutId;
                let roReasons = ro.reasons;
                let ruleOut = tree.getRuleOut(roId);
                if (!ruleOut) // TODO: Again, this is a case where an action eventually redirects back to a question
                    return;
    
                if (ruleOut["reasons"]) {
                    roReasons.forEach(reason => ruleOut["reasons"].push(reason));
                    checkForSameQuestionDuplicates(ruleOut["reasons"]);
                } else {
                    ruleOut["reasons"] = roReasons;
                }
                ruleOut["reasons"] = eliminateDuplicateReasons(ruleOut["reasons"]);
                if (ro.isApplied) { // Only if the rule is true. This is done for when we have a R/O rule: {and: [x,y]} but only x isTrue - we want to add x as a reason, but not to add the ruleout itself, because the GLOBAL rule doesn't apply.
                    lastAppliedRuleOuts.push(ruleOut);
                }
            });
    
            removeNonMustRuleOuts(lastAppliedRuleOuts);
    
            lastAppliedRuleOuts = [...new Set(lastAppliedRuleOuts)];
            lastAppliedRuleOuts.forEach(ruleOut => {
                // Only if this is the first time adding the action/rule-out
                let previousActions = questionStates[getQuestionBeforePreviousId()]; // The state before answering this question
                if (previousActions && previousActions.visitedNodes && previousActions.visitedNodes.includes(ruleOut.id))
                    return;
    
                // TODO: THE FUUCK IS THIS
                if (ruleOut)
                if (ruleOut.mode === "rule-out") {
                    // allRuleOutsTexts += ruleOut.questionStr + delimiter;
                    if(ruleOut.alert) {
                        ruleOut["type"] = "alert";
                    }
                    else {
                        ruleOut["type"] = "rule-out";
                        ruleOutTexts.push(ruleOut.questionStr);
                        // TODO: Do we want a popup for alerts?
                        // ruleOutTexts.push(ruleOut.questionStr);
                    } 
                } else if (ruleOut.mode === "action") {
                    ruleOut["type"] = "action";
                    let message = ruleOut.questionStr;
                    if (ruleOut.message) {
                        message = ruleOut.message;
                    }
                    // allActionsTexts += message + delimiter;
                    actionsTexts.push(message);
                }
            });
        };

        if (showPopups) {
            ruleOutTexts = [...new Set(ruleOutTexts)];
            ruleOutTexts.forEach(t => allRuleOutsTexts += t+delimiter);
            actionsTexts = [...new Set(actionsTexts)];
            actionsTexts.forEach(t => allActionsTexts += t+delimiter);
            if (allRuleOutsTexts != "") {
                allRuleOutsTexts = allRuleOutsTexts.substring(0, allRuleOutsTexts.length-(delimiter.length));
                showRuleOutsPopup();
            } else {
                hideRuleOutsPopup();
            }
            if (allActionsTexts != "") {
                allActionsTexts = allActionsTexts.substring(0, allActionsTexts.length-(delimiter.length));
                showActionsPopup();
            } else {
                hideActionsPopup();
            }
        }
        summary.addRuleOuts(lastAppliedRuleOuts);
    }

    function removeNonMustRuleOuts(ruleOuts) {
        let ruleOutsToRemove = [];
        ruleOuts.forEach(ro => {
            if (ro.mode === 'rule-out' || ro.type === 'rule-out' || ro.mode == 'alert' || ro.type == 'alert') { // Dafuq?
                if (!ro.reasons) {
                    ruleOutsToRemove.push(ro);
                }
                else {
                    let foundMust = false;
                    ro.reasons.forEach(reason => {
                        // TODO: Should we check the subrule strength as well? where can the "strength" parameter be?
                        if (reason.strength === RuleOutProbability.RuleOutProbability.MUST) {
                            foundMust = true;
                        }
                    });

                    if (!foundMust) {
                        ruleOutsToRemove.push(ro);
                    }
                }
            } else {
                // console.log("guh");
            }
        });

        ruleOutsToRemove.forEach(toRemove => {
            let index = ruleOuts.indexOf(toRemove);
            if (index != -1) {
                ruleOuts.splice(index, 1);
            }
        });
    }

    /**
     * This function is called when we back up and change our answer, and both answers (old and new) were a reason for a R/O.
     * This function removes the old reason.
     */
    function checkForSameQuestionDuplicates(reasons) {
        let questionIdToReasonsMap = {};
        // reasons = [...new Set(reasons)];
        reasons.sort(function(a, b) { // Sort so that the reasons that have strength will be first
            if (a.strength || a.strength==0) {
                return b.strength || b.strength==0? 0 : -1;
            } 
            else if (b.strength || b.strength==0) {
                return a.strength || a.strength==0? 0 : 1;
            }
            return 0;
        });
        reasons.sort(function(a, b) { // After sorting so that the reasons with strength are first, sort in descending order of strength (so that if we have MUST and STRONG on the same condition, it will first set MUST)
            let aStrength = 0;
            let bStrength = 0;
            if (a.strength)
                aStrength = a.strength;
            if (b.strength)
                bStrength = b.strength;
    
            return bStrength - aStrength;
        });

        
        reasons.forEach(reason => {
            if (reason.condition) {
                let questionKey = reason.condition.questionId;
                if (tree.getInput(questionKey)) { // If it's an Input, the key is the parameter itself, and not the question (which is a parameter group)
                    questionKey = reason.condition.questionId+"_"+reason.condition.answer;
                }

                if (!questionIdToReasonsMap[questionKey]) {
                    questionIdToReasonsMap[questionKey] = [];
                } 
                questionIdToReasonsMap[questionKey].push(reason);
            }
        });

        for (let key in questionIdToReasonsMap) {
            let questionReasons = questionIdToReasonsMap[key];
            if (!questionReasons)
                continue;

            // if (tree.getInput(key))
            //     continue; // Don't remove duplicates for inputs?

            let actualAnswers = summary.get(key);
            if (actualAnswers) {
                let reasonsToRemove = [];
                if (actualAnswers.selectedAnswers) {
                    questionReasons.forEach(reasonAnswer => {
                        let found = false;
                        actualAnswers.selectedAnswers.forEach(actualAnswer => {
                            if (!reasonAnswer.condition.not && actualAnswer.str === reasonAnswer.condition.answer ||
                                reasonAnswer.condition.not && actualAnswer.str !== reasonAnswer.condition.answer) { // Looking for the answer that is consistent with this reason.
                                found = true;
                            }
                        });

                        if (!found) {
                            let isEmptyAnswerAndNotCondition = (actualAnswers.selectedAnswers.length == 0 && reasonAnswer.condition.not)
                            if (!isEmptyAnswerAndNotCondition) {
                                // console.log("FOUND A DUPLICATE ARRAY-ARRAY: " + reasonAnswer);
                                reasonsToRemove.push(reasonAnswer);
                            }
                        }
                    });
                } else {
                    questionReasons.forEach(reasonAnswer => {
                        if (actualAnswers.answer && reasonAnswer.condition && 
                            ((!reasonAnswer.condition.not && actualAnswers.answer.str !== reasonAnswer.condition.answer) ||
                            (reasonAnswer.condition.not && actualAnswers.answer.str === reasonAnswer.condition.answer))) { // Looking for a reason that isn't consistent with our answers
                            // console.log("FOUND A DUPLICATE THAT SHOULDNT BE HERE!!!!!!!!! VALUE-VALUE: " + reasonAnswer.condition.answer);
                            reasonsToRemove.push(reasonAnswer);
                        }
                    });
                }

                reasonsToRemove.forEach(toRemove => {
                    let index = reasons.indexOf(toRemove);
                    if (index != -1) {
                        reasons.splice(index, 1);
                    }
                });
            }
        }
    }

    function eliminateDuplicateReasons(reasons) {
        let uniqueReasons = [];
        reasons = [...new Set(reasons)];
        reasons.sort(function(a, b) { // Sort so that the reasons that have strength will be first
            if (a.strength || a.strength==0) {
                return b.strength || b.strength==0? 0 : -1;
            } 
            else if (b.strength || b.strength==0) {
                return a.strength || a.strength==0? 0 : 1;
            }
            return 0;
        });
        reasons.sort(function(a, b) { // After sorting so that the reasons with strength are first, sort in descending order of strength (so that if we have MUST and STRONG on the same condition, it will first set MUST)
            let aStrength = 0;
            let bStrength = 0;
            if (a.strength)
                aStrength = a.strength;
            if (b.strength)
                bStrength = b.strength;
    
            return bStrength - aStrength;
        });
        reasons.forEach((r) => {
            if (!uniqueReasons.find(unique => isSameReason(unique, r))) {
                uniqueReasons.push(r);
            }
        });
        return uniqueReasons;
    }

    function isSameReason(a, b) {
        return a.condition && b.condition && a.condition.questionId === b.condition.questionId && a.condition.answer === b.condition.answer
    }


    function evaluateActionsRecursively(startingQuestion) {
        let appliedActionsIds = actionEvaluator.evaluateActions(startingQuestion);
        for (index in appliedActionsIds) {
            let a = appliedActionsIds[index];
            if (!a)
                continue;

            let actionId = a.ruleOutId;

            // TODO: Check if we already have this R/O. if we do, add the reasons. I guess this will cause a bug when we go back and remove the MUST.. it will still show probably
            // let isMust = summary.isNodeVisited(actionId);
            let isMust = false;
            let isAction = false;
            let isRuleOut = false;
            let action = tree.getRuleOut(actionId);

            if (!action) { // TODO: This is a case where an action eventually redirects back to a question
                console.log("Action not found in evaluateActionsRecursively?!");
                continue;
            }

            if (action.type == "rule-out" || action.mode == "rule-out" || action.type == "alert" || action.mode == "alert") { // TODO: dafuq?
                isRuleOut = true;
                if (a.reasons) {
                    a.reasons.forEach(reason => {
                        if (reason.strength == RuleOutProbability.RuleOutProbability.MUST) {
                            isMust = true;
                        }
                    });
                }
            } else if (action.type == "action" || action.mode == "action") {
                isAction = true;
            }

            if (a.isApplied && (isMust || isAction)) {
                summary.setVisitedNode(actionId, true); // Visited the outer-nodes
                if (isRuleOut) {
                    let ruleOutScore = calculateRuleOutScore(action);
                    if ((ruleOutScore || ruleOutScore==0) && action.score) {
                        action.score.value = ruleOutScore;
                    }
                }

                let innerActionIds = evaluateActionsRecursively(action);
                innerActionIds.forEach(innerActionId => {
                    appliedActionsIds.push(innerActionId);
                    summary.setVisitedNode(innerActionId.ruleOutId, true); // Visited the nested nodes
                });
            }
            // else if (!a.isApplied && (isMust)) { // When there's a must, but the must 
            //     let innerActionIds = evaluateActionsRecursively(action);
            //     innerActionIds.forEach(innerActionId => {
            //         appliedActionsIds.push(innerActionId);
            //         // TODO: ?
            //         // summary.setVisitedNode(innerActionId.ruleOutId, true); // Visited the nested nodes
            //     });
            // }
        }
        return appliedActionsIds;
    }

    // TODO: This is a duplicate of the function in questionController... make this some module
    function calculateRuleOutScore(ruleOut) {
        if (!ruleOut.score || !ruleOut.score.values || ruleOut.score.values.length == 0)
            return;

        let score = 0;
        ruleOut.score.values.forEach(v => {
            if (!v.rule || !v.value)
                return;
            
            if (ruleEvaluator.isRuleTrue(v.rule)) {
                score += v.value;
            }
        });
        return score;
    }

    function showRuleOutsPopup() {
        if (!settings.getShowRuleOutsPopup())
            return;

        let popup = document.getElementById("ruleOutsPopup");
        showPopup(popup, allRuleOutsTexts);
    }

    function showActionsPopup() {
        let popup = document.getElementById("actionsPopup");
        showPopup(popup, allActionsTexts);
    }

    function hideRuleOutsPopup(forceHide) {
        let popup = document.getElementById("ruleOutsPopup");
        hidePopup(popup, forceHide, allRuleOutsTexts);
    }

    function hideActionsPopup(forceHide) {
        let popup = document.getElementById("actionsPopup");
        hidePopup(popup, forceHide, allActionsTexts);
    }

    function showPopup(popup, popupText) {
        let isShowing = false;
        for (let itemIndex in popup.classList) {
            if (popup.classList[itemIndex] === "show") {
                isShowing = true;
            }
        }
        if (!isShowing && popupText && popupText != "") {
            popup.classList.toggle("show");
        }

    }

    function hidePopup(popup, forceHide, popupText) {
        let isShowing = false;
        for (let itemIndex in popup.classList) {
            if (popup.classList[itemIndex] === "show") {
                isShowing = true;
            }
        }
        if (isShowing) {
            if (!popupText || popupText == "" || forceHide) {
                popup.classList.toggle("show");
            }
        }
    }

    let allRuleOutsTexts = "";
    let allActionsTexts = "";
    function getRuleOutsPopupText() {
        return allRuleOutsTexts;
    }
    function getActionsPopupText() {
        return allActionsTexts;
    }

    function back() {
        visitedQuestions.pop();
        let previousQuestionId = getPreviousQuestionId();

        if (previousQuestionId !== undefined) {
            // if (questionStates[$scope.current.id]) { // Clear the state
            //     questionStates[$scope.current.id] = null;
            //     delete questionStates[$scope.current.id];
            // }
            summary.clearAnswers($scope.current); // TODO: This makes the user type in everything again..
            $scope.current = tree.getQuestion(previousQuestionId);
            $scope.end = $scope.current.end;
            $scope.summary = null;
            summary.setVisitedNode($scope.current.id, false);

            if ($scope.current.baseQuestionId) {
                console.log("Backed into a side-chain");
                let baseQuestion = tree.getQuestion($scope.current.baseQuestionId);
                sideChainNavigator.setState(baseQuestion);
            }

            let baseQuestion = sideChainNavigator.getState();
            if (baseQuestion) {
                baseQuestion.clearTraversedSideChainsWithHistory(visitedQuestions); // When we're backing-out from a question in the side-chain, clear the base question's traversed side chains values
                $scope.current.baseQuestionId = baseQuestion.id; // Set the base quesiton
            } else {
                $scope.current.clearTraversedSideChains(); // Set alreadyTraversed=false on all the side-chain rules of this question - we backed out
            }


            let beforePreviousSnapshotIndex = getQuestionBeforePreviousId(); // We backed into this question - restore its snapshot (which is essentially the snapshot after answer the previous question of the question we just backed-out into)
            if (beforePreviousSnapshotIndex) {
                let beforePreviousSnapshot = questionStates[beforePreviousSnapshotIndex];
                if (beforePreviousSnapshot) {
                    summary.restoreSnapshot({actions:beforePreviousSnapshot.actions, visitedNodes:beforePreviousSnapshot.visitedNodes});
                }
            } else { // We have backed-out into the first question
                summary.restoreSnapshot({}); // TODO: Is this ok? just override everything?
            }
            submitInputsWithoutPopups(); // Restoring the snapshot will override the visitedNodes & actions that came from answering Inputs in the middle of the questionere. so run all the input rules again after this (and don't show the popups... derp)

            // Clean & restore selected answers.
            // summary.clearAnswers($scope.current);
        }
    }

    function getPreviousQuestionId() {
        if (visitedQuestions.length == 0)
            return "0";

        return visitedQuestions[visitedQuestions.length-1]; // Peek
    }

    function getQuestionBeforePreviousId() {
        if (visitedQuestions.length <= 1)
            return;

        return visitedQuestions[visitedQuestions.length-2]; // Peek
    }

    function performAction(item) {
        let allAppliedRuleOuts = summary.getRuleOuts();
        if (!allAppliedRuleOuts)
            return;

        let action = allAppliedRuleOuts.find(a => a.id == item.id);
        if (!action)
            return;

        action.isConfirmed = true;
        generateSummary();
    }

    function printSummary() {
        generateSummary();
    }

    function generateSummary() {
        $scope.summary = medicalSummary.generate();
    }

    function showSummary() {
        generateSummary();
        showModal("myModal");
    }

    function showInputsDialog() {
        showModal("inputs-modal", submitInputsWithPopups);
    }

    function showSettingsDialog() {
        showModal("settings-modal");
    }

    let selectedItem;
    function getItemData() {
        if (selectedItem && selectedItem.reasons && selectedItem.reasons.length > 0) {
            return selectedItem.reasons.sort(function(r1,r2) {
                console.log(r1);
            });
        }
    }
    function getItemName() {
        if (selectedItem && selectedItem.name)
            return selectedItem.name;
        else
            return "";
    }
    function getItemScore() {
        if (selectedItem && (selectedItem.score || selectedItem.score==0)) {
            return selectedItem.score;
        }
        return null;
    }

    function getRuleOutSummary() {
        if (shouldShowRuleOutSummary()) {
            return ruleOutSummary.generate(selectedItem.originalNode);
        }
        return null;
    }

    function shouldShowRuleOutSummary() {
        return selectedItem && selectedItem.originalNode.ruleOutText;
    }

    function getStrengthTextClass(item) {
        let strengthText = item.strengthText;
        if (!strengthText)
            return "";

        switch (strengthText) {
            case RuleOutProbability.WEAK_TEXT:
                return "weak-strength-text";
            case RuleOutProbability.STRONG_TEXT:
                return "strong-strength-text";
            case RuleOutProbability.VERY_STRONG_TEXT:
                return "very-strong-strength-text";
            case RuleOutProbability.MUST_TEXT:
                return "must-strength-text";
        }
    }
    
    function showReasons(action) {
        selectedItem = action;
        showModal("item-modal");
    }

    function hasRows(paragraph) {
        if (!paragraph.rows)
            return false;
        
        let foundNonEmptyRow = false;
        paragraph.rows.forEach(r => {
            if (r && r.length>0) {
                foundNonEmptyRow = true;
            }
        });
        return foundNonEmptyRow;
    }

    let allQuestionsAnswers = [];
    function showAnswersSummary() {
        allQuestionsAnswers = getAllQuestions();
        showModal("answers-summary-modal");
    }
    function showMultiAnswerSummary(question) {
        $scope.selectedMultiAnswerQuestion = question;
        showModal("multi-answer-summary-modal");
    }

    function getAnswersSummary() {
        return allQuestionsAnswers;
    }

    function getAllQuestions() {
        let questionToAnswerMap = [];
        let questions = tree.getAllQuestions();
        for (let key in questions) {
            let q = questions[key];
            let a = answerRetriever.getAnswer(key);
            let questionToPush;
            if (!a) // TODO: This will continue for answers that we have not answered yet, but will also continue for empty answers..
                continue;

            if (Array.isArray(a)) {
                let answers = [];
                let unselectedAnswers = [];

                // All answers
                q.answers.forEach(answer => {
                    if (answer && answer.str) {
                        if (answer.selected) {
                            answers.push(answer.str);
                        } else {
                            unselectedAnswers.push(answer.str);
                        }
                    }
                });

                // if (answers.length > 0) {
                    questionToPush = {answers: answers, unselectedAnswers: unselectedAnswers};
                // }
            } else {
                questionToPush = {answer: a};
            }
            questionToPush.questionStr = q.questionStr;
            questionToPush.doctorDisplayText = q.doctorDisplayText;
            questionToPush.shouldDisplayForDoctor = q.shouldDisplayForDoctor;
            questionToAnswerMap.push(questionToPush);
        }
        return questionToAnswerMap
    }

    function showModal(modalId, onCloseCallback) {
        var modal = document.getElementById(modalId);
        var span = document.getElementsByClassName(modalId+"-close")[0];
        modal.style.display = "block";

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
            if (onCloseCallback) {
                onCloseCallback();
            }
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
                if (onCloseCallback) {
                    onCloseCallback();
                }
            }
        }
    }

    function isNextDisabled() {
        let currentQuestion = $scope.current;
        if (!currentQuestion)
            return false;

        if (currentQuestion.mode==consts.questionMode.MULTI_CHOICE && currentQuestion.multiChoiceYesNo) {
            return $scope.current.answers.find(a => {
                return !a.isAnswered;
            });
        }
        return false;
    }

    inputWeight = {};
    inputWeight["-39"] = 100;
    inputWeight["-51"] = 99;
    inputWeight["-43"] = 98;
    inputWeight["-64"] = 97;
    inputWeight["-47"] = 96;
    inputWeight["-53"] = 95;
    inputWeight["-100"] = 94;

    function getInputParameters() {
        let allInputs = tree.getAllInputs();
        for (let key in allInputs) {
            parameterGroup = allInputs[key];
            parameterGroup.answers.forEach(parameter => {
                let ngModelName = getNgModelName(parameterGroup, parameter);
                if (!$scope.inputParameters[ngModelName]) {
                    if (parameter.type === "free-text") {
                        $scope.inputParameters[ngModelName] = "";
                    } else if (parameter.type === "checkbox") {
                        $scope.inputParameters[ngModelName] = false;
                    }else if (parameter.type === "radio") {
                        $scope.inputParameters[ngModelName] = undefined;
                    }
                }
            });
        };

        let orderedInputs = Object.values(allInputs).sort((a,b) => {
            a_weight = inputWeight[a.id] || 0;
            b_weight = inputWeight[b.id] || 0;
            return b_weight - a_weight;
        });
        return orderedInputs;
    }

    function getNgModelName(parameterGroup, parameter) {
        let ngModelName = parameterGroup.questionStr+"@@@_@@@"+parameter.str;
        return ngModelName;
    }

    function onPressedSubmitInputDialog() {
        closeModal("inputs-modal");
        submitInputsWithPopups();
    }

    function closeModal(modalId) {
        var modal = document.getElementById(modalId);
        modal.style.display = "none";
    }

    function submitInputsWithPopups() {
        submitInputs(true);
    }

    function submitInputsWithoutPopups() {
        submitInputs(false);
    }

    function submitInputs(showPopups) {
        let allInputs = tree.getAllInputs();
        for (let key in allInputs) {
            parameterGroup = allInputs[key];
            parameterGroup.answers.forEach(parameter => {
                let ngModelName = getNgModelName(parameterGroup, parameter);
                let paramValue = $scope.inputParameters[ngModelName];
                if (parameter.type === 'checkbox') {
                    if (paramValue == "")
                        paramValue = false;

                    if (paramValue || paramValue == false) {
                        parameter.value = paramValue;
                    }
                } else if (parameter.type === 'free-text') {
                    if (paramValue) {
                        parameter.value = paramValue;
                    }
                } else if (parameter.type === 'radio') {
                    if (paramValue) {
                        parameter.value = paramValue;
                    }
                } else {
                    console.log("unknown input parameter type: " + parameter.type);
                }
            });
        };

        // After setting all the values, run the rules
        if (showPopups) {
            allRuleOutsTexts = "";
            allActionsTexts = "";
            lastAppliedRuleOuts = [];
        }
        applyRuleOutsForMultipleQuestions(allInputs, showPopups);
        // if (showPopups) {
        //     $scope.$apply(); // Refresh the screen because it doesn't get the change
        // }
    }

    function getAppSettings() {
        return settings.getSettingsObject();
    }

    function shouldDisplayActionConfirmButton(action) {
        return !settings.getCruiseControl();
    }

    const RADIO_TITLE_DELIMITER = "::";
    const RADIO_VALUES_DELIMITER = ";";
    function getRadioTitle(parameterString) {
        let endIndex = parameterString.indexOf(RADIO_TITLE_DELIMITER);
        if (endIndex != -1) {
            return parameterString.substring(0, endIndex);
        }
    }

    function getRadioValues(parameterString) {
        let valuesStartIndex = parameterString.indexOf(RADIO_TITLE_DELIMITER);
        let valuesString = "";
        if (valuesStartIndex != -1) {
            valuesString = parameterString.substring(valuesStartIndex+RADIO_TITLE_DELIMITER.length);
        }

        return valuesString.split(RADIO_VALUES_DELIMITER);
    }
    
    ctor();
}

/***/ }),

/***/ "./src/question/questionDirective.js":
/*!*******************************************!*\
  !*** ./src/question/questionDirective.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const questionTemplate = __webpack_require__(/*! ./question.html */ "./src/question/question.html");
const angularModule = __webpack_require__(/*! ../angular/angularModule */ "./src/angular/angularModule.js");
angularModule.directive('question', questionDirective);

function questionDirective() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {},
        template: questionTemplate,
        controller: 'questionController',
        controllerAs: 'questionController'
    }
}

/***/ }),

/***/ "./src/question/sideChainNavigator.js":
/*!********************************************!*\
  !*** ./src/question/sideChainNavigator.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


let state; // TODO: This should be a stack/queue of states.. but.. oke
function setState(stateBeforeSideChain) {
    state = stateBeforeSideChain;
}

function getState() {
    return state;
}

module.exports = {setState, getState}

/***/ }),

/***/ "./src/ruleEvaluator.js":
/*!******************************!*\
  !*** ./src/ruleEvaluator.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let summary = __webpack_require__(/*! ./summary/summary */ "./src/summary/summary.js")
let tree = __webpack_require__(/*! ./tree/tree */ "./src/tree/tree.js")

function getNextQuestionId(question) {
    let rule = getFirstAppliedRule(question);
    if (rule !== undefined) {
        return rule.nextId;
    } else {
        return question.defaultNextId;
    }
}

function getFirstAppliedRule(question) {
    return question.rules && question.rules.find(isRuleTrue);
}

function isRuleTrue(rule) {
    if (rule.type == "side-chain" && rule.alreadyTraversed) {
        return false;
    }
    rule.alreadyTraversed = true;

    if (rule.subRules) {
        if (rule.logic == "and")
            return rule.subRules.every(isRuleTrue);
        if (rule.logic == "or")
            return rule.subRules.find(isRuleTrue) != undefined;
    }

    let evaluation = true;
    let visitedNode;
    if (rule.logic == "visited node" && rule.visitedNode) {
        visitedNode = rule.visitedNode;
        evaluation = summary.isNodeVisited(visitedNode.nodeKey);
        if (visitedNode.not) {
            evaluation = !evaluation;
        }
        return evaluation;
    }

    let answerMap = summary.getMap();
    let condition = rule.condition;
    if (condition) {
        answerObj = answerMap[condition.questionId];
        // if (answerObj) {
        //     if (condition.operator) { // Operator
        //         let answer = parseInt(answerObj.answer.str);
        //         let operatorValue = parseInt(condition.operatorValue);
        //         if (condition.operator === ">") {
        //             evaluation = answer > operatorValue;
        //         } else if (condition.operator === ">=") {
        //             evaluation = answer >= operatorValue;
        //         } else if (condition.operator === "<") {
        //             evaluation = answer < operatorValue;
        //         } else if (condition.operator === "<=") {
        //             evaluation = answer <= operatorValue;
        //         } else if (condition.operator === "=") {
        //             evaluation = answer == operatorValue;
        //         }
        //     }
        //     else { // Regular answer condition
        //         if (answerObj.selectedAnswers) {
        //             evaluation = answerObj.selectedAnswers.find(a => {
        //                 return a.str === condition.answer;
        //             });
        //         } else {
        //             evaluation = answerObj.answer.str === condition.answer;
        //         }
        //     }
        // } else {
        //     evaluation = false;

        //     // let ruleOut = tree.getRuleOut(condition.questionId);
        //     // if (ruleOut) { // Found the rule-out
        //     //     if (ruleOut.score) {
        //     //         answerObj = {answer: {str: ruleOut.score.value}};
        //     //     }
        //     // }
        // }

        if (!answerObj) {
            evaluation = false;
            let ruleOut = tree.getRuleOut(condition.questionId);
            if (ruleOut) { // Found the rule-out - a value of a rule out is not the answer (because it doesn't exist), but the ruleout's score.
                if (ruleOut.score) {
                    let scoreValue;
                    if (ruleOut.score.value || ruleOut.score.value == 0) {
                        scoreValue = ruleOut.score.value;
                    }
                    answerObj = {answer: {str: scoreValue}};
                }
            }
        }

        let inputGroup = tree.getInput(condition.questionId);
        if (inputGroup) { // Found the inputGroup - the value will be the value field for a specific answer
            let parameter = inputGroup.answers.find(a => a.str === condition.answer);
            if (parameter) {
                if (parameter.type === 'checkbox') {
                    if (parameter.value) {
                        answerObj = {answer: {str: parameter.str}}; // Act as if the user has answered this answer.
                    }
                } else if (parameter.type === 'free-text' || parameter.type === 'radio') {
                    if (parameter.type == 'radio' && !parameter.value) {
                        // radio is not filled.
                    } else {
                        answerObj = {answer: {str: parameter.value}};
                    }
                } else {
                    console.log("ruleEvaluator: Unknown input parameter type: " + parameter.type);
                }
            }
        }

        evaluation = isConditionSatisfied(answerObj, condition, evaluation);

        if (!answerObj) { // If there's no answer - this is false.
        //     if (condition.not) // NOT something, and we answered empty.
        //         return true;
        //     else 
                return false;
        }
    }

    if (condition && condition.not) {
        evaluation = !evaluation;
    }

    return evaluation;
}

function isConditionSatisfied(answerObj, condition, evaluation) {
    if (!answerObj)
        return evaluation;

    if (condition.operator) { // Operator
        let answer = parseValueToInt(answerObj.answer.str);
        if (Number.isNaN(answer)) {
            answer = answerObj.answer.str;
        }
        let operatorValue = parseValueToInt(condition.operatorValue);
        if (Number.isNaN(operatorValue)) {
            operatorValue = condition.operatorValue;
        }
        if (condition.operator === ">") {
            evaluation = answer > operatorValue;
        } else if (condition.operator === ">=") {
            evaluation = answer >= operatorValue;
        } else if (condition.operator === "<") {
            evaluation = answer < operatorValue;
        } else if (condition.operator === "<=") {
            evaluation = answer <= operatorValue;
        } else if (condition.operator === "=") {
            evaluation = answer == operatorValue;

        }
    } else { // Regular answer condition
        if (answerObj.selectedAnswers) {
            evaluation = answerObj.selectedAnswers.find(a => {return a.selected && a.str === condition.answer}) != undefined;
        } else {
            evaluation = answerObj.answer.str === condition.answer;
        }
    }
    return evaluation;
}

function parseValueToInt(value) {
    let answer;
    try {
        answer = parseInt(value);
    } catch(err) {
        let index = value.indexOf("  -") // Just in case they added another space.. idk
        if (index == -1) {
            let index = value.indexOf(" -")
        }
        if (index == -1) {
            index = value.indexOf("-")
        }
        if (index != -1) {
            let trimmedAnswer = value.substring(0,index);
            answer = parseInt(trimmedAnswer);
        }
    }
    return answer;
}

module.exports = {getFirstAppliedRule, isRuleTrue}

/***/ }),

/***/ "./src/settings.js":
/*!*************************!*\
  !*** ./src/settings.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

function toggleCruiseControl() {
    cruiseControlSetting.value = !cruiseControlSetting.value;
}

function getCruiseControl() {
    return cruiseControlSetting.value;
}

function toggleShowRuleOutsPopup() {
    showRuleOutsPopupSetting.value = !showRuleOutsPopupSetting.value;
}

function getShowRuleOutsPopup() {
    return showRuleOutsPopupSetting.value;
}

const CRUISE_CONTROL_PARAMETER = "Cruise Control";
const DISPLAY_RULE_OUTS_PARAMETER = "Display Rule Outs Popup";

let cruiseControlSetting = {value: false, name: CRUISE_CONTROL_PARAMETER, onChanged: toggleCruiseControl};
let showRuleOutsPopupSetting = {value: true, name: DISPLAY_RULE_OUTS_PARAMETER, onChanged: toggleShowRuleOutsPopup};
let settings = [cruiseControlSetting, showRuleOutsPopupSetting];

function getSettingsObject() {
    return settings;
}

module.exports = {getSettingsObject, getShowRuleOutsPopup, getCruiseControl, toggleCruiseControl, toggleShowRuleOutsPopup};




/***/ }),

/***/ "./src/summary/RuleOutProbability.js":
/*!*******************************************!*\
  !*** ./src/summary/RuleOutProbability.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

const RuleOutProbability = {
    VERY_WEAK: 0,
    WEAK: 1,
    NEUTRAL: 2,
    STRONG : 3,
    VERY_STRONG : 4,
    MUST: 5
};

const WEAK_TEXT = "סבירות נמוכה";
const STRONG_TEXT = "סבירות בינונית";
const VERY_STRONG_TEXT = "סבירות גבוהה";
const MUST_TEXT = "סבירות גבוהה מאוד";
const UNKNOWN_STRENGTH_TEXT = "סבירות לא ידועה";

function get(ordinal) {
    switch (ordinal) {
        case 0:
            return RuleOutProbability.VERY_WEAK;
        case 1:
            return RuleOutProbability.WEAK;
        case 2:
            return RuleOutProbability.NEUTRAL;
        case 3:
            return RuleOutProbability.STRONG;
        case 4:
            return RuleOutProbability.VERY_STRONG;
        case 5:
            return RuleOutProbability.MUST;
    }
}

module.exports={RuleOutProbability, get, WEAK_TEXT, STRONG_TEXT, VERY_STRONG_TEXT, MUST_TEXT, UNKNOWN_STRENGTH_TEXT};

/***/ }),

/***/ "./src/summary/answerRetriever.js":
/*!****************************************!*\
  !*** ./src/summary/answerRetriever.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const summary = __webpack_require__(/*! ./summary */ "./src/summary/summary.js");
const tree = __webpack_require__(/*! ../tree/tree */ "./src/tree/tree.js");

function getAnswer(id) {
    if (!summary.get(id)) {
        return null; // TODO: ?
    }
    
    if (summary.get(id).answer) {
        return summary.get(id).answer.str;
    } else if (summary.get(id).selectedAnswers) {
        return summary.get(id).selectedAnswers;
    }

    // TODO: If id is of a ruleout or alert - return the score.
    let ruleOut = summary.getRuleOuts(id);
    if (ruleOut && ruleOut.score && (ruleOut.score.value || ruleOut.score.value==0)) {
        return ruleOut.scorer.value;
    }

    return null;  // TODO: ?
}

function getAllQuestionsAndAnswers() {
    return tree.getAllQuestions();
}

function getAllRuleOuts() {
    return summary.getRuleOuts();
}

module.exports = {getAnswer, getAllQuestionsAndAnswers, getAllRuleOuts};

/***/ }),

/***/ "./src/summary/inputRetriever.js":
/*!***************************************!*\
  !*** ./src/summary/inputRetriever.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const tree = __webpack_require__(/*! ../tree/tree */ "./src/tree/tree.js");

function getInput(id) {
    return tree.getInput(id);
}

function getAllInputs() {
    return tree.getAllInputs();
}

module.exports = {getInput, getAllInputs};

/***/ }),

/***/ "./src/summary/medicalSummary.js":
/*!***************************************!*\
  !*** ./src/summary/medicalSummary.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const anamnesisStoryTemplate = __webpack_require__(/*! ./template.txt */ "./src/summary/template.txt").default;
const definitiveDiagnosis = __webpack_require__(/*! ./paragraphs/definitiveDiagnosis */ "./src/summary/paragraphs/definitiveDiagnosis.js");
const answerRetriever = __webpack_require__(/*! ./answerRetriever */ "./src/summary/answerRetriever.js");
const inputRetriever = __webpack_require__(/*! ./inputRetriever */ "./src/summary/inputRetriever.js");
const ruleEvaluator = __webpack_require__(/*! ../ruleEvaluator */ "./src/ruleEvaluator.js");
const summary = __webpack_require__(/*! ./summary */ "./src/summary/summary.js");
const inputState = __webpack_require__(/*! ../inputs/inputState */ "./src/inputs/inputState.js");

function generate() {
    let allRuleOuts = definitiveDiagnosis.createRuleOuts();
    let ruleOuts = allRuleOuts.filter(ro => !ro.confirmed && !ro.moveToTop);
    let topRuleOuts = allRuleOuts.filter(ro => ro.moveToTop);
    let confirmedRuleOuts = allRuleOuts.filter(ro => ro.confirmed);
    return {
        ruleOuts, topRuleOuts, confirmedRuleOuts,
        alerts: definitiveDiagnosis.createAlerts(), 
        anamnesisStory: createAnamnesisStory(),
        actionSections: createActionParagraphs(definitiveDiagnosis.createActions())
    };
}

const ANSWER_VALUE_TEMPLATE_PREFIX = "[val=";
const ANSWER_VALUE_TEMPLATE_SUFFIX = "]";
const VALUE_DELIMITER = ", ";
const SENTENCE_DELIMITER = " ";
const ROW_DELIMITER = " ";

function createActionParagraphs(actions) {
    let actionParagraphs = {
        "בדיקה גופנית": {name: "בדיקה גופנית" , actions: []},
        "בדיקות מעבדה": {name: "בדיקות מעבדה" , actions: []},
        "דימות": {name: "דימות" , actions: []},
        "בדיקות אחרות" : {name: "בדיקות אחרות"  , actions: []},
        "טיפול": {name: "טיפול" , actions: []},
    }
    
    actions.forEach(action => {
        if (actionParagraphs[action.actionSection])
            actionParagraphs[action.actionSection].actions.push(action);
    });

    return actionParagraphs;
}

function createAnamnesisStory() {
    let storyRows = {};

    addInitialAnamnesis(storyRows);

    let inputs = inputRetriever.getAllInputs();
    for (let key in inputs) {
        let input = inputs[key];
        if (!input.anamnesis) 
            continue;

        input.anamnesis.forEach(anamnesis => addAnamnesis(storyRows, anamnesis));
    }

    // TODO: Maybe go over all visited nodes instead? there can be an anamnesis for R/O and Actions too..
    let questions = answerRetriever.getAllQuestionsAndAnswers();
    for (let key in questions) {
        let q = questions[key];
        if (!q.anamnesis)
            continue;

        q.anamnesis.forEach(anamnesis => addAnamnesis(storyRows, anamnesis));
    }

    let ruleOuts = answerRetriever.getAllRuleOuts(); // This is also actions and alrets
    for (let key in ruleOuts) {
        let ro = ruleOuts[key];
        if (!ro.anamnesis)
            continue;

        ro.anamnesis.forEach(anamnesis => addAnamnesis(storyRows, anamnesis));
    }

    for (let key in storyRows) {
        let row = storyRows[key];

        // Check if the suffix is the sentense delimiter.
        let rowSuffix = row.substring(row.length-SENTENCE_DELIMITER.length, row.length)
        if (rowSuffix == SENTENCE_DELIMITER) {
            row = row.substring(0, row.length-(SENTENCE_DELIMITER.length)); // Trim the last sentence delimiter
        }

        // Add the row delimiter
        storyRows[key] = row+ROW_DELIMITER;
    }

    return formatStoryRows(storyRows);
}

function addAnamnesis(storyRows, anamnesis) {
    let isApplied = true;
    // TODO: This is for empty rules with no condition - we can pretty confidently remove this when shiloh fixes the rules
    // if (anamnesis.rule && ((anamnesis.rule.condition && anamnesis.rule.condition.questionId) || !anamnesis.rule.condition)) { 
    if (anamnesis.rule) {
        isApplied = ruleEvaluator.isRuleTrue(anamnesis.rule);
    }

    if (isApplied) {
        let text = anamnesis.diagnosis;
        text = insertMissingValues(text)

        if (text && anamnesis.section) {
            if (!storyRows[anamnesis.section]) {
                storyRows[anamnesis.section] = "";
            }
            let delimiterText = text.trim().length > 0? SENTENCE_DELIMITER : "";
            storyRows[anamnesis.section] += text + delimiterText;
        }
    }
}

//TODO: this doess not support multiple values
function insertMissingValues(text) {
    if (!text.includes(ANSWER_VALUE_TEMPLATE_PREFIX)) {
        return text;
    }
    let startIndex = text.indexOf(ANSWER_VALUE_TEMPLATE_PREFIX);
    let endIndex = text.indexOf(ANSWER_VALUE_TEMPLATE_SUFFIX);
    let valuesToExtract = text.substring(startIndex+ANSWER_VALUE_TEMPLATE_PREFIX.length, endIndex).split(',');
    let questionId = valuesToExtract[0];
    let inputIndex = valuesToExtract[1];
    
    if (inputIndex) {
        val = getInputValue(questionId, inputIndex);
    } else {
        val = getQuestionValue(questionId);
    }

    if (val) {
        return text.substring(0, startIndex) + val + text.substring(endIndex + 1, text.length);
    }
}

function getInputValue(questionId, inputIndex) {
    let input = inputState.get();
}

function getQuestionValue(questionId) {
    let answer = answerRetriever.getAnswer(questionId);
    if (answer === undefined || answer === null) {
        return;
    }
    let answerText = "";
    if (Array.isArray(answer)) {
        answer.forEach(a => {
            if (a.selected) {
                answerText += a.str + VALUE_DELIMITER;
            }
        });
        answerText = answerText.substring(0, answerText.length-(VALUE_DELIMITER.length));
    } else {
        answerText = answer;
    }
    return answerText;
}

const MAIN_COMPLAINT_TITLE = "תלונה עיקרית";
const MAIN_COMPLAINT_1 = "תלונה עיקרית -שורה 1";
const MAIN_COMPLAINT_2 = "תלונה עיקרית -שורה 2";
const MAIN_COMPLAINT_3 = "תלונה עיקרית -שורה 3";

const COMPLAINT_BACKGROUND_TITLE = "רקע לתלונה";
const COMPLAINT_BACKGROUND_1 = "רקע לתלונה -שורה 1";
const COMPLAINT_BACKGROUND_2 = "רקע לתלונה -שורה 2";
const COMPLAINT_BACKGROUND_3 = "רקע לתלונה -שורה 3";
const COMPLAINT_BACKGROUND_4 = "רקע לתלונה -שורה 4";

const MEDICAL_BACKGROUND = "רקע רפואי";
const MEDICAL_BACKGROUND_DISPLAY_NAME = "רקע רפואי ומחלות"
const ALERGIES = "אלרגיות";
const MEDICATIONS = "תרופות";
const FORMER_HOSPITALIZATIONS = "אישפוזים קודמים";
const SOCIAL_BACKGROUND = "רקע סוציאלי";
const FAMILY_BACKGROUND = "רקע משפחתי";
const DD_HIGH = "אבחנה מבדלת - סבירות גבוהה";
const DD_LOW = "אבחנה מבדלת - סבירות נמוכה";
const RECOMMENDATIONS = "המלצות";
const BODY_CHECK = "בדיקה גופנית";
const LEB_TESTS = "בדיקות מעבדה";
const SCAN = "דימות";
const OTHER_CHECKS = "בדיקות אחרות";

function formatStoryRows(storyRows) {
    let mainComplaintParagraph = {title: MAIN_COMPLAINT_TITLE, rows: []};
    mainComplaintParagraph.rows.push(storyRows[MAIN_COMPLAINT_1]);
    mainComplaintParagraph.rows.push(storyRows[MAIN_COMPLAINT_2]);
    mainComplaintParagraph.rows.push(storyRows[MAIN_COMPLAINT_3]);

    let complaintBackgroundParagraph = {title: COMPLAINT_BACKGROUND_TITLE, rows: []};
    complaintBackgroundParagraph.rows.push(storyRows[COMPLAINT_BACKGROUND_1]);
    complaintBackgroundParagraph.rows.push(storyRows[COMPLAINT_BACKGROUND_2]);
    complaintBackgroundParagraph.rows.push(storyRows[COMPLAINT_BACKGROUND_3]);
    complaintBackgroundParagraph.rows.push(storyRows[COMPLAINT_BACKGROUND_4]);

    let paragraphs = [
        mainComplaintParagraph, 
        complaintBackgroundParagraph, 
    ];

    addParagraph(MEDICAL_BACKGROUND, storyRows, paragraphs)
    addParagraph(ALERGIES, storyRows, paragraphs)
    addParagraph(MEDICATIONS, storyRows, paragraphs)
    addParagraph(FORMER_HOSPITALIZATIONS, storyRows, paragraphs)
    addParagraph(SOCIAL_BACKGROUND, storyRows, paragraphs)
    addParagraph(FAMILY_BACKGROUND, storyRows, paragraphs)
    addParagraph(DD_LOW, storyRows, paragraphs)
    addParagraph(DD_LOW, storyRows, paragraphs)
    addParagraph(RECOMMENDATIONS, storyRows, paragraphs)
    addParagraph(BODY_CHECK, storyRows, paragraphs)
    addParagraph(LEB_TESTS, storyRows, paragraphs)
    addParagraph(SCAN, storyRows, paragraphs)
    addParagraph(OTHER_CHECKS, storyRows, paragraphs)

    eliminateUndefinedRows(paragraphs);
    return paragraphs;
}

function addParagraph(name, storyRows, paragraphs) {
    paragraph = {title: name, rows: []};
    paragraph.rows.push(storyRows[name]);
    paragraphs.push(paragraph);
}

// Eliminate rows that are undefined or empty (because it breaks the angular HTML)
function eliminateUndefinedRows(paragraphs) {
    paragraphs.forEach(p => {
        if (!p.rows)
            return;
        
        let allRowsOk = false;
        while (!allRowsOk) {
            let rowIndexesToRemove = [];
            for (let index in p.rows) {
                let r = p.rows[index];
                if (!r || r.trim().length==0) {
                    rowIndexesToRemove.push(index);
                }
            }

            if (rowIndexesToRemove.length == 0) {
                allRowsOk = true;
            } else {
                p.rows.splice(rowIndexesToRemove[0], 1); // we can only delete 1 index at a time..
            }
        }
    });
}

function formatByName(str, formats) {
    Object.keys(formats).forEach(name => {
        let val = formats[name];
        str = str.replace('$'+name+'$', val);
    })
    return str;
}

function addInitialAnamnesis(storyRows) {
    let state = inputState.get();
    let age = state['נתונים דמוגרפיים@@@_@@@גיל'];
    let gender = state['נתונים דמוגרפיים@@@_@@@מין::זכר;נקבה'];
    if (!age) {
        return;
    }
    if (gender == "זכר")
        storyRows[MAIN_COMPLAINT_1] = `גבר בן ${age}` + " ";
    else if (gender == "נקבה")
        storyRows[MAIN_COMPLAINT_1] = `אישה בת ${age}` + " ";
}


module.exports = {generate}

/***/ }),

/***/ "./src/summary/paragraphs/definitiveDiagnosis.js":
/*!*******************************************************!*\
  !*** ./src/summary/paragraphs/definitiveDiagnosis.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

let stroke = __webpack_require__(/*! ./stroke */ "./src/summary/paragraphs/stroke.js");
const answerRetriever = __webpack_require__(/*! ../answerRetriever */ "./src/summary/answerRetriever.js");
const ruleOutsRetriever = __webpack_require__(/*! ../ruleOutsRetriever */ "./src/summary/ruleOutsRetriever.js");
const tree = __webpack_require__(/*! ../../tree/tree */ "./src/tree/tree.js")
const RuleOutProbability = __webpack_require__(/*! ../RuleOutProbability */ "./src/summary/RuleOutProbability.js");
const ruleEvaluator = __webpack_require__(/*! ../../ruleEvaluator */ "./src/ruleEvaluator.js");

function createAlerts() {
    return create("alert");
}

function createRuleOuts() {
    return create("rule-out");
}

function createActions() {
    return create("action").sort(function(action1, action2){
        return action1.order - action2.order;
    });
}

function create(type) {
    let ruleOuts = [];
    let allRuleOuts = ruleOutsRetriever.getRuleOuts();
    for (let index in allRuleOuts) {
        let ruleOut = allRuleOuts[index];

        if (ruleOut.type != type)
            continue;

        let strengthScore = calculateStrength(ruleOut);
        let strengthText = getStrengthDescription(strengthScore);
        
        let reasons = [];
        let questionToRelevantAnswersMap = {}
        if (ruleOut.reasons) {
            ruleOut.reasons.forEach(reason => {
                if (reason.displayReason) {
                    addReason(reason, questionToRelevantAnswersMap);
                }
            }); 
        }
        let delimiter = ", ";
        for (let questionStr in questionToRelevantAnswersMap) {
            relevantAnswers = questionToRelevantAnswersMap[questionStr];

            let relevantAnswersText = "";
            relevantAnswers.forEach(a => relevantAnswersText += a + delimiter);
            relevantAnswersText = relevantAnswersText.substring(0, relevantAnswersText.length-delimiter.length);
            
            reasons.push({questionStr: questionStr, answerStr: relevantAnswersText});
        }

        let scoreObject = undefined;
        if (ruleOut.score && (ruleOut.score.value || ruleOut.score.value==0)) {
            // This is because we don't have dummy-proofing. we calculate again in case anything changed since last time
            let ruleOutScore = calculateRuleOutScore(ruleOut);
            if ((ruleOutScore || ruleOutScore==0) && ruleOut.score) {
                ruleOut.score.value = ruleOutScore;
            }

            // scoreValue = ruleOut.score.value;
            scoreObject = ruleOut.score;
        }

        ruleOuts.push({id: ruleOut.id, name: ruleOut.questionStr, reasons: reasons, strengthScore: strengthScore, strengthText: strengthText, score: scoreObject, isConfirmed: ruleOut.isConfirmed, originalNode: ruleOut, order: ruleOut.actionOrder, actionSection: ruleOut.actionSection, confirmed: ruleOut.confirmed, moveToTop: ruleOut.moveToTop});
    }

    ruleOuts.sort(function(a, b) {
        let aStrength = 0;
        let bStrength = 0;
        if (a.strengthScore)
            aStrength = a.strengthScore;
        if (b.strengthScore)
            bStrength = b.strengthScore;

        return bStrength - aStrength;
    });
    return ruleOuts;
}

// TODO: This is a duplicate of the function in questionController... make this some module
function calculateRuleOutScore(ruleOut) {
    if (!ruleOut.score || !ruleOut.score.values || ruleOut.score.values.length == 0)
        return;

    let questionToRelevantAnswersMap = {};
    let questionToScoreAdditionMap = {};
    let score = 0;
    ruleOut.score.values.forEach(v => {
        if (!v.rule || !v.value)
            return;
        
        if (ruleEvaluator.isRuleTrue(v.rule)) {
            score += v.value;
            addReason(v.rule, questionToRelevantAnswersMap);
            addScoreAmount(v.rule, questionToScoreAdditionMap, v.value);
        }
    });

    ruleOut.score.reasons = [];
    let delimiter = ", ";
    for (let questionStr in questionToRelevantAnswersMap) {
        relevantAnswers = questionToRelevantAnswersMap[questionStr];

        let relevantAnswersText = "";
        relevantAnswers.forEach(a => relevantAnswersText += a + delimiter);
        relevantAnswersText = relevantAnswersText.substring(0, relevantAnswersText.length-delimiter.length);
        
        let scoreAmount = questionToScoreAdditionMap[questionStr];
        ruleOut.score.reasons.push({questionStr: questionStr, answerStr: relevantAnswersText, scoreAmount: scoreAmount});
    }

    return score;
}

function addScoreAmount(reason, questionToScoreAdditionMap, scoreAmountToAdd) {
    if (reason.condition) {
        let condition = reason.condition;
        let question = tree.getQuestion(condition.questionId);
        let operatorText = "";
        
        if (condition.operator) { // Operator
            let operatorValue = parseInt(condition.operatorValue);
            if (condition.operator === ">") {
                operatorText += "גדול מ-"
            } else if (condition.operator === ">=") {
                operatorText += "גדול או שווה ל-"
            } else if (condition.operator === "<") {
                operatorText += "קטן מ-"
            } else if (condition.operator === "<=") {
                operatorText += "קטן או שווה ל-"
            } else if (condition.operator === "=") {}
        }

        let isInput = false;
        let isRuleOut = false;
        if (!question) {
            question = tree.getInput(condition.questionId);
            if (question) {
                isInput = true;
            }
        }
        if (!question) {
            question = tree.getRuleOut(condition.questionId);
            if (question) {
                isRuleOut = true;
            }
        }

        if (question) {
            let questionStr = question.doctorDisplayText || question.questionStr;
            questionToScoreAdditionMap[questionStr] = scoreAmountToAdd;
        }
    }
    else if (reason.logic == "visited node" && reason.visitedNode) {
        let nodeId = reason.visitedNode.nodeKey;
        let node = tree.getQuestion(nodeId);
        if (!node) {
            node = tree.getInput(nodeId);
        }
        if (!node) {
            node = tree.getRuleOut(nodeId);
        }

        if (node) {
            let questionStr = node.doctorDisplayText || node.questionStr;
            if (reason.visitedNode.not) {
                questionStr = "אין " + questionStr;
            }
            if (questionToScoreAdditionMap[questionStr]) {
                questionToScoreAdditionMap[questionStr] += scoreAmountToAdd;
            } else {
                questionToScoreAdditionMap[questionStr] = scoreAmountToAdd;
            }
        }
    }
}

function calculateStrength(ruleOut) {
    // TODO: What?
    if (ruleOut.strength || ruleOut.strength == 0)
        return ruleOut.strength;

    let strengthCounterMap = {};
    strengthCounterMap[RuleOutProbability.RuleOutProbability.VERY_WEAK] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.WEAK] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.NEUTRAL] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.STRONG] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.VERY_STRONG] = 0;
    strengthCounterMap[RuleOutProbability.RuleOutProbability.MUST] = 0;
    let foundStrength = false;
    if (ruleOut.reasons) {
        ruleOut.reasons.forEach(reason => {
            if (reason.strength || reason.strength==0) {
                let strengthOrdinal = reason.strength;
                let strengthEnum = RuleOutProbability.get(strengthOrdinal);
                if (strengthCounterMap[strengthEnum] || strengthCounterMap[strengthEnum] == 0) {
                    strengthCounterMap[strengthEnum]++;
                    foundStrength = true;
                }
            }
        }); 
    }

    if (!foundStrength)
        return null;

    let finalStrengthScore = strengthCounterMap[RuleOutProbability.RuleOutProbability.VERY_WEAK]*(VERY_WEAK_SCORE) +
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.WEAK]*(WEAK_SCORE) +
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.NEUTRAL]*NEUTRAL_SCORE +
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.STRONG]*STRONG_SCORE +
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.VERY_STRONG]*VERY_STRONG_SCORE + 
                                strengthCounterMap[RuleOutProbability.RuleOutProbability.MUST]*MUST_SCORE;
    return finalStrengthScore;
}

// const WEAK_SCORE = -10;
// const STRONG_SCORE = 10;
// const VERY_STRONG_SCORE = 20;
// const MUST_SCORE = 100;

const VERY_WEAK_SCORE = -20;
const WEAK_SCORE = -10;
const NEUTRAL_SCORE = 0;
const STRONG_SCORE = 10;
const VERY_STRONG_SCORE = 22;
const MUST_SCORE = 50;

// TODO:
// const VERY_HIGH_MIN_SCORE = 70;
const VERY_HIGH_MIN_SCORE = 100;
const HIGH_MIN_SCORE = 51;
const MEDIUM_MIN_SCORE = 41;

function getStrengthDescription(strengthScore) {
    if (!strengthScore)
        return null;

    if (strengthScore < MEDIUM_MIN_SCORE) // lower than MEDIUM_MIN_SCORE (low)
        return RuleOutProbability.WEAK_TEXT;

    if (strengthScore < HIGH_MIN_SCORE) // between MEDIUM_MIN_SCORE and HIGH_MIN_SCORE (medium)
        return RuleOutProbability.STRONG_TEXT;

    if (strengthScore < VERY_HIGH_MIN_SCORE) // between HIGH_MIN_SCORE and VERY_HIGH_MIN_SCORE (high)
        return RuleOutProbability.VERY_STRONG_TEXT;

    if (strengthScore >= VERY_HIGH_MIN_SCORE) // higher than VERY_HIGH_MIN_SCORE (very high)
        return RuleOutProbability.MUST_TEXT;

    return RuleOutProbability.UNKNOWN_STRENGTH_TEXT;


    // if (score < STRONG_SCORE) // between weak and strong
    //     return RuleOutProbability.WEAK_TEXT;
    
    // if (score < VERY_STRONG_SCORE) // between strong and very strong
    //     return RuleOutProbability.STRONG_TEXT;

    // if (score < MUST_SCORE) // between very strong and must
    //     return RuleOutProbability.VERY_STRONG_TEXT;

    // if (score >= MUST_SCORE)
    //     return RuleOutProbability.MUST_TEXT;
}

function addReason(reason, questionToRelevantAnswersMap) {
    if (reason.condition) {
        // TODO: for dont display reasons
        // TODO: if (reason.condition.dontDisplay) return;

        let condition = reason.condition;
        let question = tree.getQuestion(condition.questionId);
        let operatorText = "";
        let suffixText = "";
        // if (condition.not && condition.operator) {
        //     operatorText += "לא ";
        // } else if (condition.not) {
        //     operatorText += "שונה מ-";
        // }

        // if (condition.not) {
        //     operatorText += "לא ";
        // }
        
        // if (condition.operator) { // Operator
        //     let operatorValue = parseInt(condition.operatorValue);
        //     if (condition.operator === ">") {
        //         operatorText += "גדול מ-"
        //     } else if (condition.operator === ">=") {
        //         operatorText += "גדול או שווה ל-"
        //     } else if (condition.operator === "<") {
        //         operatorText += "קטן מ-"
        //     } else if (condition.operator === "<=") {
        //         operatorText += "קטן או שווה ל-"
        //     } else if (condition.operator === "=") {}
        // }

        let isInput = false;
        let isRuleOut = false;
        if (!question) {
            question = tree.getInput(condition.questionId);
            if (question) {
                isInput = true;
            }
        }
        if (!question) {
            question = tree.getRuleOut(condition.questionId);
            if (question) {
                isRuleOut = true;
            }
        }

        if (question) {
            let answer = condition.answer; // TODO: This isn't really good, we don't write "he answered 6", we write "he answered more than 5".. 
            let questionStr = question.doctorDisplayText || question.questionStr;

            if (!answer || condition.operator/* || condition.not*/) {
                // if (condition.operator) {
                //     operatorText += condition.operatorValue + " (";
                // } 
                
                answer = answerRetriever.getAnswer(question.id);

                if (!answer && isInput) { // Let's pretend this is an Input
                    let parameter = question.answers.find(a => a.str === condition.answer)
                    if (parameter) {
                        answer = parameter.value;
                        questionStr = parameter.str;
                        if (parameter.type == "radio") {
                            questionStr = getRadioTitle(questionStr);
                        }
                    }
                }

                if (Array.isArray(answer)) {
                    let answerText = "";
                    let delimiter = ", ";
                    answer.forEach(a => answerText += a.str+delimiter)
                    answerText = answerText.substring(0, answerText.length-delimiter.length);
                    answer = answerText;
                }
                //suffixText = ")"
            }

            if (condition.not) {
                answer = answerRetriever.getAnswer(question.id);
                if (Array.isArray(answer)) { // TODO: This is stupido
                    let answerText = "";
                    let delimiter = ", ";
                    answer.forEach(a => answerText += a.str+delimiter)
                    answerText = answerText.substring(0, answerText.length-delimiter.length);
                    answer = answerText;
                }
            }

            if (answer == null) {
                answer = "";
            }

            let actualDescriptionText = operatorText+answer;//+suffixText;
            if (condition.operator && condition.operator === "=") { // If it's an "=" operator, we have an operator but it's straight-forward - we don't need the "(value)" text
                actualDescriptionText = answer;
            }

            if (questionToRelevantAnswersMap[questionStr] && !condition.not) { // NOT goes through all the array, so we don't need it to happen twice\
                questionToRelevantAnswersMap[questionStr].push(actualDescriptionText);
                // questionToRelevantAnswersMap[questionStr] = [...new Set(questionToRelevantAnswersMap[questionStr])];
            } else {
                questionToRelevantAnswersMap[questionStr] = [actualDescriptionText];
            }
        } else { // We got here from a ruleout/action
            // TODO: Maybe just take the reasons for the ruleout/action that brought us here?
            console.log("Got a Rule/Action with no condition: logic=" + reason.logic + ", condition={questionId="+condition.questionId+", answer=" + condition.answer + "}");
        }
    } 
    else if (reason.logic == "visited node" && reason.visitedNode) {
        let nodeId = reason.visitedNode.nodeKey;
        let node = tree.getQuestion(nodeId);
        if (!node) {
            node = tree.getInput(nodeId);
        }
        if (!node) {
            node = tree.getRuleOut(nodeId);
        }

        if (node) {
            let questionStr = node.questionStr;
            if (reason.visitedNode.not) {
                questionStr = "אין " + node.questionStr;
            }
            questionToRelevantAnswersMap[questionStr] = []; // Just add the visited node name and that's it
        }
    }
    else {
        if (reason.logic == "or") {
            if (reason.subRules && reason.subRules.length > 0) {
                // addReason(reason.subRules[0], questionToRelevantAnswersMap); // This is "or" - just add the first one
                // TODO: "or" should show everything?
                reason.subRules.forEach(subRule => addReason(subRule, questionToRelevantAnswersMap));
            }
        } else if (reason.logic == "and") {
            if (reason.subRules) {
                reason.subRules.forEach(subRule => addReason(subRule, questionToRelevantAnswersMap));
            }
        } else {
            // TODO: logic == "default" ? what should we do in this case
            if (reason.logic != "default") {
                console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ no condition on the reason? - " + reason.logic)
            }
        }
    }
}

// TODO: This is copied from questionController - make a module out of this and re-use
const RADIO_TITLE_DELIMITER = "::";
function getRadioTitle(parameterString) {
    let endIndex = parameterString.indexOf(RADIO_TITLE_DELIMITER);
    if (endIndex != -1) {
        return parameterString.substring(0, endIndex);
    }
}

// //אבחנה מבדלת
// function create() {
//     let low = "סבירות נמוכה- ";
//     let high = "סבירות גבוהה- ";

//     let strokebranch = answerRetriever.getAnswer("strokebranch");
//     if (strokebranch == "לא") {
//         high += "TIA, ";
//     }

//     if (stroke.isStroke()) {
//         low +="גידול מוחי, wernicke-encephalopathy, hepathic-encephalopathy, אבצס מוחי, מחלה מטוכונדריאלית, היפוגליקמיה, ";
//         high += "שבץ-cva, ";
//     }

//     let g1_1 = answerRetriever.getAnswer("g1_1");
//     if (g1_1 == "כן") {
//         low += "ספסיס, ";
//     } 

//     let g1_2 = answerRetriever.getAnswer("g1_2");
//     if (g1_2 == "כן") {
//         low += "גיליאן ברה, ";
//     } 

//     let g1_5 = answerRetriever.getAnswer("g1_5");
//     if (g1_5 == "כן") {
//         low += "מנינגיטיס, ";
//     }

//     return high + "\n" + low;
// }

module.exports = {createRuleOuts, createActions, createAlerts};

/***/ }),

/***/ "./src/summary/paragraphs/stroke.js":
/*!******************************************!*\
  !*** ./src/summary/paragraphs/stroke.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const answerRetriever = __webpack_require__(/*! ../answerRetriever */ "./src/summary/answerRetriever.js");

function create() {
    let story = "";
    let hadStroke = "";
    let without = "";
    let extra = "";
    let qFAST1 = answerRetriever.getAnswer("qFAST1");
    if (qFAST1 == "כן") {
        story += "מתלונן על חולשה ביד/רגל/פנים, ";
    }

    let qFAST2 = answerRetriever.getAnswer("qFAST2");
    if (qFAST2 == "כן") {
        story += "המיפארזיס, ";
    }

    let qFAST3 = answerRetriever.getAnswer("qFAST3");
    if (qFAST3 == "כן") {
        story += "דיספזיה, ";
    }

    let strokebranch = answerRetriever.getAnswer("strokebranch");
    if (strokebranch == "כן") {
        story += "משך הסימפטומים יותר מ24 שעות, ";
    } else if (strokebranch == "לא"){
        story += "משך הסימפטומים פחות מ24 שעות, ";
    }
    
    let q2000_1 = answerRetriever.getAnswer("q2000_0");
    if (q2000_1 == "כן") {
        story += "החל באופן פתאומי, ";
    } else if (q2000_1 == "לא") {
        story += "החל באופן הדרגתי, ";
    }

    let q2000_0 = answerRetriever.getAnswer("q2000_0");
    if (q2000_0 == "כן") {
        story += "דיספגיה, ";
    } else if (q2000_0 == "לא") {
        without += "ללא דיספגיה, ";
    }

    let q2000 = answerRetriever.getAnswer("q2000");
    if (q2000 == "כן") {
        story += "היה פרכוס, ";
    } else if (q2000 == "לא") {
        without += "ללא פרכוס, ";
    }

    let q2001 = answerRetriever.getAnswer("q2001");
    if (q2001 == "כן") {
        story += "מדמם כעת, ";
    } else if (q2001 == "לא") {
        without += "ללא דימום חיצוני, ";
    }

    let q2002 = answerRetriever.getAnswer("q2002");
    if (q2002 == "כן") {
        story += "עם אירוע של איבוד ההכרה, ";
    } else if (q2002 == "לא") {
        without += "ללא איבוד הכרה,  ";
    }

    let q2003 = answerRetriever.getAnswer("q2003");
    if (q2003 == "כן") {
        story += "מדווח על כאב ראש, ";
    } else if (q2003 == "לא") {
        without += "שולל כאב ראש,  ";
    }

    let q2010 = answerRetriever.getAnswer("q2010");
    if (q2010 == "כן") {
        story += "יש פוטופוביה, ";
    } else if (q2010 == "לא") {
        without += "ללא פוטופוביה, ";
    }

    let q2011 = answerRetriever.getAnswer("q2011");
    if (q2011 == "כן") {
        story += "יש נוקשות צווארית, ";
    } else if (q2011 == "לא") {
        without += "שולל נוקשות צווארית, ";
    }

    let q2012 = answerRetriever.getAnswer("q2012");
    if (q2012 == "כן") {
        story += "יש בחילה, ";
    } else if (q2012 == "לא") {
        without += "ללא בחילה, ";
    }

    let q2006 = answerRetriever.getAnswer("q2006");
    if (q2006 == "כן") {
        hadStroke += "היה אירוע מוחי בעבר, ";
    } else if (q2006 == "לא") {
        hadStroke += "שולל אירוע מוחי בעבר, ";
    }

    let q2008 = answerRetriever.getAnswer("q2008");
    if (q2008 == "לא") {
        without += "שולל הפרעות בקצב לב, ";
    }

    let q2009 = answerRetriever.getAnswer("q2009");
    if (q2009 == "לא") {
        story += "שולל ממחלות קרישת דם, ";
    }

    let q2014 = answerRetriever.getAnswer("q2014");
    if (q2014 == "כן") {
        hadStroke += "עבר ניקור מותני בשבוע האחרון, ";
    } else if (q2014 == "לא") {
        hadStroke += "לא עבר ניקור מותני בשבוע האחרון,  ";
    }
    
    let q2015 = answerRetriever.getAnswer("q2015");
    if (q2015 == "לא") {
        without += "שולל אירוע לבבי ופריקרדיטיס בעבר, ";
    }

    let q2016 = answerRetriever.getAnswer("q2016");
    if (q2016 == "לא") {
        without += "אינו נוטל מדללי דם או נוגדי טסיות, ";
    }

    let q2017 = answerRetriever.getAnswer("q2017");
    if (q2017 == "לא") {
        without += "שולל דימום פנימי בשישה שבועות האחרונים, ";
    }

    let g1_1 = answerRetriever.getAnswer("g1_1");
    if (g1_1 == "כן") {
        story += " יש חום/בלבול, ";
    } else if (g1_1 == "לא") {
        without += "ללא חום ללא בלבול,  ";
    }

    let g1_2 = answerRetriever.getAnswer("g1_2");
    if (g1_2 == "כן") {
        story += " מתלונן על קלקול קיבה, ";
    } else if (g1_2 == "לא") {
        without += "שולל קלקול קיבה, ";
    }

    let g1_3 = answerRetriever.getAnswer("g1_3");
    if (g1_3 == "כן") {
        story += "יש תלונות אורינריות, ";
    } else if (g1_3 == "לא") {
        without += "ללא צריבה במתן שתן, ";
    }

    let g1_5 = answerRetriever.getAnswer("g1_5");
    if (g1_5 == "כן") {
        story += "יש פריחה עורית, ";
    } else if (g1_5 == "לא") {
        without += "ללא פריחה עורית, ";
    }
    
    let g1_14 = answerRetriever.getAnswer("g1_14");
    if (g1_14 == "כן") {
        story += "מיאלגיה, ";
    } else if (g1_14 == "לא")  {
        extra += "ללא מיאלגיה, ";
    }

    let g1_16 = answerRetriever.getAnswer("g1_16");
    if (g1_16 == "כן") {
        story += "יש זיהום, ";
    } else if (g1_16 == "לא") {
        without += "שולל זיהום, ";
    }
    
    let g1_0 = answerRetriever.getAnswer("g1_0");
    if (g1_0 == "סיבה אחרת") {
        extra = "\nבנוסף, מתלונן על " + answerRetriever.getAnswer("g1_0_1") + ", ";
    } else if (g1_0 && g1_0 != "סיבה אחרת") {
        extra = "\nבנוסף, מתלונן על, " +g1_0;
    }
    
    let g1_4 = answerRetriever.getAnswer("g1_4");
    if (g1_4 == "לא") {
        without += "ללא רקע של דיכוי חיסוני, ";
    }

    let g1_6 = answerRetriever.getAnswer("g1_6");
    if (g1_6 == "כן") {
        story += "נסיעה לחול לאחרונה, ";
    } else if (g1_6 == "לא"){
        without += "ללא נסיעה לחול לאחרונה, ";
    }

    let g1_7 = answerRetriever.getAnswer("g1_7");
    if (g1_7 == "לא") {
        without += "ללא רקע של מייסתניה גרוויס, ";
    }

    let g1_8 = answerRetriever.getAnswer("g1_8");
    if (g1_8 == "לא") {
        without += "ללא רקע של מחלת כליות, ";
    }

    let g1_9 = answerRetriever.getAnswer("g1_9");
    if (g1_9 == "כן") {
        extra += "חיסונים כשגרה, ";
    } else if (g1_9 == "לא") {
        extra += "ללא חיסונים כשגרה, ";
    }

    let g1_13 = answerRetriever.getAnswer("g1_13");
    if (g1_13 == "לא") {
        without += "ללא רקע של מחלות בבלוטת התריס, ";
    }

    return story + hadStroke + "\n\n" + without + extra;
}

function isStroke() {
    let qFAST1 = answerRetriever.getAnswer("qFAST1");
    let qFAST2 = answerRetriever.getAnswer("qFAST2");
    let qFAST3 = answerRetriever.getAnswer("qFAST3");
    return (qFAST1 == "כן" || qFAST2 == "כן" || qFAST3 == "כן");
}

module.exports = {create, isStroke};

/***/ }),

/***/ "./src/summary/ruleOutSummary.js":
/*!***************************************!*\
  !*** ./src/summary/ruleOutSummary.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const ruleEvaluator = __webpack_require__(/*! ../ruleEvaluator */ "./src/ruleEvaluator.js");

const SENTENCE_DELIMITER = " ";

function generate(ruleOut) {
    let sentencesToApply = ruleOut.ruleOutText.filter(ruleOutText => {
        return ruleEvaluator.isRuleTrue(ruleOutText.rule);
    });

    let summary = "";
    sentencesToApply.forEach(ruleOutText => {
        summary += ruleOutText.text + SENTENCE_DELIMITER;
    });
    return summary
}

module.exports = {generate};

/***/ }),

/***/ "./src/summary/ruleOutsRetriever.js":
/*!******************************************!*\
  !*** ./src/summary/ruleOutsRetriever.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const summary = __webpack_require__(/*! ./summary */ "./src/summary/summary.js");

function getRuleOuts() {
    return summary.getRuleOuts();
}

module.exports = {getRuleOuts};

/***/ }),

/***/ "./src/summary/summary.js":
/*!********************************!*\
  !*** ./src/summary/summary.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const consts = __webpack_require__(/*! ../consts */ "./src/consts.js");

let answers = {};
let ruleOuts = []; // This is the actions as well
let visitedNodes = [];

function restoreAnswers(question, restoredAnswers) {
    if (Array.isArray(restoredAnswers)) {
        answers[question.id] = {question: question, selectedAnswers: restoredAnswers};
    } else {
        answers[question.id] = {question: question, answer: restoredAnswers};
    }

    // Set a.selected and a.isAnswered on the answers of the question as well
    if (!question.answers)
        return;

    if (Array.isArray(restoredAnswers)) {
        question.answers.forEach(old => {
            restoredAnswers.forEach(restored => {
                if (old.str == restored.str) {
                    old.selected = restored.selected;
                    old.isAnswered = restored.isAnswered;
                }
            }) 
        });
    } else {
        question.answers.forEach(old => {
            if (restoredAnswers.str == old.str) {
                old.selected = restoredAnswers.selected;
                old.isAnswered = restoredAnswers.isAnswered;
            }
        });
    }
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

function clearAnswers(question) {
    let answer = answers[question.id];
    if (!answer)
        return;

    let allAnswers = answer.question.answers;
    if (allAnswers) {
        let newAnswers = [];
        allAnswers.forEach(a => {
            let newAnswerObj = clone(a);
            newAnswerObj.selected = false;
            newAnswerObj.isAnswered = false;
            newAnswers.push(newAnswerObj);
        });
        answer.question.answers = newAnswers;
    }
    else if (answer.answer) {
        // answer.answer.selected = false;
        answer.answer = null;
    }

    answers[question.id] = null;
    delete answers[question.id];
}

function setEmptyAnswer(question) {
    answers[question.id] = {question: question, selectedAnswers: []};
}

function append(question, answer) {
    if (question.mode==consts.questionMode.MULTI_CHOICE) {
        let selectedAnswers;
        if (answers[question.id]) {
            selectedAnswers = answers[question.id].selectedAnswers;
            if (answer.selected || question.multiChoiceYesNo) { // Add answer
                selectedAnswers.push(answer);
                selectedAnswers = [...new Set(selectedAnswers)];
            } else { // Remove answer
                const index = selectedAnswers.indexOf(answer);
                if (index > -1) {
                    selectedAnswers.splice(index, 1);
                }
            }
        } else {
            selectedAnswers = [answer];
            answers[question.id] = {question, selectedAnswers};
        }
        answers[question.id] = {question, selectedAnswers};
        
    }
    else {
        answers[question.id] = {question, answer};
    }
    //console.log(print());
}

function get(id) {
    if (id) {
        return answers[id];
    }
}

function getMap() {
    return answers;
}

function getRuleOuts() {
    return ruleOuts;
}

function restoreSnapshot(snapshot) {
    setRuleOuts(snapshot.actions);
    setVisitedNodes(snapshot.visitedNodes);
}

// TODO: This whole "new set" thing only worked for IDs.. now it's not useful at all
// TODO: If you change this shit to a map (for R/O strength) - make sure you change the questionController snapshots as well!
function setRuleOuts(newRuleOuts) {
    ruleOuts = [...new Set(newRuleOuts)];
}

function addRuleOuts(ruleOutsToAdd) {
    ruleOutsToAdd.forEach(ro => ruleOuts.push(ro));
    ruleOuts = [...new Set(ruleOuts)];
}

function addRuleOut(ruleOut) {
    ruleOuts.push(ruleOut);
    ruleOuts = [...new Set(ruleOuts)];
}

function removeRuleOut(ruleOut) {
    let index = ruleOuts.indexOf(ruleOut);
    if (index != -1) {
        ruleOuts.splice(index, 1);
    }
}

function setVisitedNodes(newVisitedNodes) {
    visitedNodes = [...new Set(newVisitedNodes)];
}

function setVisitedNode(nodeId, isVisited) {
    if (isVisited) {
        visitedNodes.push(nodeId);
    }
    else { // Remove visited node
        const index = visitedNodes.indexOf(nodeId);
        if (index > -1) {
            visitedNodes.splice(index, 1);
        }
    }
}

function getVisitedNodes() {
    return visitedNodes;
}

function isNodeVisited(nodeId) {
    return visitedNodes.includes(nodeId);
}

function print() {
    let str = "";
    Object.keys(answers).forEach(questionId => {
        let item = answers[questionId];
        str += `\n${item.question.questionStr}:${item.answer.str}`;
    });
    return str;
}

module.exports = {append, clearAnswers, setEmptyAnswer, get, getMap, print, addRuleOuts, addRuleOut, removeRuleOut, setVisitedNode, isNodeVisited, getRuleOuts, getVisitedNodes, restoreSnapshot, restoreAnswers}

/***/ }),

/***/ "./src/summary/template.txt":
/*!**********************************!*\
  !*** ./src/summary/template.txt ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("תלונה עיקרית:\r\n$age$ $socialMain$\r\nהגיע בשל $mainComplaint$\r\n$gcs$ \r\n\r\nרקע לתלונה:\r\n$stroke$\r\n\r\nרקע רפואי:\r\n$medicalBG$\r\n$allergies$\r\n\r\nתרופות:\r\n$medicine$\r\n\r\nאשפוזים קודמים:\r\n$sergicalHistory$\r\n\r\nרקע סוציאלי:\r\n$social$\r\n\r\nרקע משפחתי:\r\n$family$\r\n\r\n$fibrinolyza_contra_indications$\r\n\r\nאבחנה מבדלת:\r\n$DD$\r\n\r\nהמלצות:\r\n$suggestions$");

/***/ }),

/***/ "./src/tree/tree.js":
/*!**************************!*\
  !*** ./src/tree/tree.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const treeBuilder = __webpack_require__(/*! ./treeBuilder */ "./src/tree/treeBuilder.js");
const graphCreator = __webpack_require__(/*! ../graph/graphCreator */ "./src/graph/graphCreator.js");
// const treeBuilder2 = require('../graph/treeBuilder2');
let questionMap;
let ruleOutMap;
let inputs;
let sideChains;

function init() {
    const inputElement = document.getElementById("load-file");
    inputElement.addEventListener("change", loadFile, false);
}

function createGraph(json) {
    // questionMap = treeBuilder.createQuestions();
    questionMap = treeBuilder.createQuestions(json);
    ruleOutMap = treeBuilder.getRuleOutMap();
    inputs = treeBuilder.getInputMap();
    //questionMap = treeBuilder2.createTree();
    let graph = graphCreator.createGraph(Object.values(questionMap));
    console.log(graph);

    // Map side-chains
    createSideChainsMapping();
}

function createSideChainsMapping() {
    sideChains = {};
    for (let key in questionMap) {
        let question = questionMap[key];
        if (!question.rules)
            continue;

        question.rules.forEach(rule => {
            let baseQuestionOfFromQuestion = getBaseQuestionId(question.id);
            if (rule.type === 'side-chain') {
                addToSideChains(question.id, rule.nextId);
            }
            else if (rule.type === 'next-question' && baseQuestionOfFromQuestion) {
                addToSideChains(baseQuestionOfFromQuestion, rule.nextId);
            }
        });
    }
}

function addToSideChains(baseQuestionId, questionId) {
    if (!sideChains[baseQuestionId]) {
        sideChains[baseQuestionId] = [];
    }
    sideChains[baseQuestionId].push(questionId);
}

function getBaseQuestionId(questionId) {
    for (let key in sideChains) {
        let sideChain = sideChains[key];
        if (sideChain.includes(questionId)) {
            return key;
        }
    }
}

function loadFile() {
    let file = this.files[0];
    let reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = loaded;
}

function loaded(evt) {
    var fileString = evt.target.result;
    let json = JSON.parse(fileString);
    createGraph(json);
}

function getAllQuestions() {
    return questionMap;
}

function getQuestion(id) {
    return questionMap[id];
}

function getRuleOut(id) {
    return ruleOutMap[id];
}

function getAllInputs() {
    return inputs;
}

function getInput(id) {
    return inputs[id];
}

function getFirst() {
    let first = Object.values(questionMap).find(question => question.isFirst);
    return first || questionMap["-1"];
}

function getLast() {
    // TODO: We can scan the tree in order to determine where it ends (where we have a question with no rules from it)
    return ["-149", "-152"];
}

module.exports = {init, getQuestion, getFirst, getRuleOut, getAllQuestions, getAllInputs, getInput, getLast, getBaseQuestionId};




/***/ }),

/***/ "./src/tree/treeBuilder.js":
/*!*********************************!*\
  !*** ./src/tree/treeBuilder.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const Question = __webpack_require__(/*! ../question/question */ "./src/question/question.js");
// const consts = require('../consts');
const RuleOutProbability = __webpack_require__(/*! ../summary/RuleOutProbability */ "./src/summary/RuleOutProbability.js");

let currentId = 1;
let questions = {};
let ruleOuts = {}; // This should be called "actions" - both ruleOuts and actions
// const GRAPH_PATH = "./data/graph.json";
let inputs = {};

function createQuestions(json) {
    let allQuestions = {};
    let allRuleOuts = {};
    let allInputs = {};

    // Add the questions
    for (let questionIndex in Object.values(json["nodeDataArray"])) {
        let questionData = json["nodeDataArray"][questionIndex];
        questionData.name = questionData.key;
        if (questionData.mode === 'rule-out' || questionData.mode === 'action' || questionData.mode === 'alert') {
            let ruleOut = createQuestion(questionData.text, questionData);
            allRuleOuts[String(questionData.key)] = ruleOut;
        }
        else if (questionData.mode === 'input' || questionData.mode === 'input') {
            // TODO: set type=questionData.inputType for every answer?
            let input = createQuestion(questionData.text, questionData);
            allInputs[String(questionData.key)] = input;
        }
        else {
            let question = createQuestion(questionData.text, questionData);
            allQuestions[String(questionData.key)] = question;
        }
    }

    // Add the rules
    for (let ruleIndex in Object.values(json["linkDataArray"])) {
        let ruleData = json["linkDataArray"][ruleIndex];
        let fromKey = String(ruleData.from);
        let fromQuestion = allQuestions[fromKey];
        let toKey = String(ruleData.to);
        let toQuestion = allQuestions[toKey];
        if (!fromKey || !toKey)
            continue;

        if (!fromQuestion && !allRuleOuts[fromKey] && !allInputs[fromKey]) // If not a valid rule from a question or an action
            continue;

        if (!fromQuestion || !toQuestion) { // Not leading to or coming from a question (R/O or action etc)
            if (!fromQuestion) { // Rule outs
                fromQuestion = allRuleOuts[fromKey];
            }
            if (!toQuestion) {
                toQuestion = allRuleOuts[toKey];
            }

            if (!fromQuestion) { // Inputs
                fromQuestion = allInputs[fromKey];
            }
            if (!toQuestion) {
                toQuestion = allInputs[toKey];
            }
        
            if (fromQuestion && toQuestion) {
                let displayReason = true;
                if (ruleData.display == false) {
                    displayReason = false;
                }
                fromQuestion.addRuleOutRule({logic: ruleData.logic, nextId: toKey, subRules: ruleData.subRules, ruleOrder: ruleData.order, strength: getProbability(ruleData.strength), displayReason: displayReason});
            }
            continue;
        }
        else {
            fromQuestion.addRule({logic: ruleData.logic, nextId: toKey, subRules: ruleData.subRules, type: ruleData.type, ruleOrder: ruleData.order});
            if (ruleData.logic === "default") {
                fromQuestion.nextQuestion(toKey);
            }
        }
    }





    // // TODO: Maybe Go over all the subrules, and for each subrule that references a question, add this ruleOutRule to that question as well.
    // for (let questionKey in allQuestions) {
    //     if (questionKey.rules)
    // }





    for (let key in allQuestions) {
        let question = allQuestions[key];
        // TODO: Remove stand-alone questions

        // if (question && (!question.defaultNextId || !question.nextId)) {
        //     delete allQuestions[key];
        // } 
        // else
        if (question) { // Sort the rules for each question
            question.sortRules();
            question.sortRuleOutRules();
        }
    }

    questions = allQuestions;
    ruleOuts = allRuleOuts;
    inputs = allInputs;
    return questions;
}

function getProbability(text) {
    switch (text) {
            case "מחליש מאוד":
                return RuleOutProbability.RuleOutProbability.VERY_WEAK;
            case "מחליש":
                return RuleOutProbability.RuleOutProbability.WEAK;
            case "ניטרלי":
                return RuleOutProbability.RuleOutProbability.NEUTRAL;
            case "מחזק":
                return RuleOutProbability.RuleOutProbability.STRONG;
            case "מחזק מאוד":
                return RuleOutProbability.RuleOutProbability.VERY_STRONG;
            case "must":
                return RuleOutProbability.RuleOutProbability.MUST;
    }
}

function createQuestion(text, options) {
    if (options.name) {
        let question = new Question(String(options.name), text, options);
        questions[options.name] = question;
        return question;
    }

    let question = new Question(String(currentId), text, options);
    questions[currentId] = question;
    currentId++;
    return question;
}

function getRuleOutMap() {
    return ruleOuts;
}

function getInputMap() {
    return inputs;
}

module.exports = {createQuestions, getRuleOutMap, getInputMap};

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map